@extends('layouts.public.master_plain')
@section('content')
<div class="login-box" style="margin:2% auto;">
  <div class="login-logo">
    <img src="{{ URL::to('logo.png') }}" alt="" style="width: 200px; height: auto">
    <a href="#"><b>Pharma</b>Tieup</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
{!! Form::open(['method' => 'POST', 'url' => '/login']) !!}
<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
    {!! Form::label('username', 'User ID') !!}
    {!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('username') }}</small>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password',['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('password') }}</small>
</div>
      <div class="row">
      	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
{!! Form::close() !!}
    
      <div class="form-group has-feedback">
{{--         <input type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div> --}}

<div class="clearfix">

</div>
<br>
    <a href="{{ route('forgotpassword') }}">I forgot my password</a><br>
    <a href="{{ route('user.index') }}" class="text-center">Register Now !</a> <br>
<b><a href="http://pharmatieup.com">Go to Homepage</a></b>  </div>
<a href="{{ route('distributorlogin') }}" class="text-center">Distributor Login</a> 

  <!-- /.login-box-body -->
</div>
@endsection