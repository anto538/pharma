@extends('layouts.public.master')
@section('content')
{{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> --}}
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Request Help </h3>
      <span class="pull-right box-tools"><a href="{{ route('store') }}" class="btn btn-large btn-block btn-primary">Back</a></span>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(['method' => 'POST', 'route' => 'gethelp', 'class' => 'form-horizontal']) !!}
        
            <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                {!! Form::label('subject', 'Subject') !!}
                {!! Form::text('subject', null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('subject') }}</small>
            </div>
            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                {!! Form::label('message', 'Message') !!}
                {!! Form::textarea('message', null, ['class' => 'form-control ', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('message') }}</small>
            </div>
        
            <div class="btn-group pull-right">
                
                {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
            </div>
        
        {!! Form::close() !!}
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</div>
{{-- </div> --}}
@endsection
