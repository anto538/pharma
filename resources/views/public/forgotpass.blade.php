@extends('layouts.public.master_plain')
@section('content')
<div class="login-box" style="margin:2% auto;">
  <div class="login-logo">
    <img src="{{ URL::to('logo.png') }}" alt="" style="width: 200px; height: auto">
    <a href="#"><b>Pharma</b>Tieup</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    @if (Session::has('message'))
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
      {{ Session::get('message') }}
    </div>
    
    @endif
    <p class="login-box-msg">Enter Your Details</p>
    {!! Form::open(['method' => 'POST', 'url' => '/forgotpassword']) !!}
    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
      {!! Form::label('username', 'Username') !!}
      {!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="text-danger">{{ $errors->first('username') }}</small>
    </div>
    <div class="form-group{{ $errors->has('bemail') ? ' has-error' : '' }}">
      {!! Form::label('bemail', 'Registered Email ID') !!}
      {!! Form::text('bemail', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="text-danger">{{ $errors->first('bemail') }}</small>
    </div>
    <div class="form-group{{ $errors->has('pan') ? ' has-error' : '' }}">
      {!! Form::label('pan', 'PAN Card No') !!}
      {!! Form::text('pan', null, ['class' => 'form-control', 'required' => 'required']) !!}
      <small class="text-danger">{{ $errors->first('pan') }}</small>
    </div>
    <div class="row">

      <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
      </div>
      <!-- /.col -->
    </div>
    {!! Form::close() !!}
    
    <div class="form-group has-feedback">
{{--         <input type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div> --}}


      {{-- <a href="#">I forgot my password</a><br> --}}
      {{-- <a href="{{ route('user.index') }}" class="text-center">Register a new membership</a> --}}

    </div>
    <!-- /.login-box-body -->
  </div>
  @endsection