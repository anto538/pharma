@extends('layouts.public.master')
@section('content')
<?php 
$gross = $vat = $cd = $net_amount = 0;
 ?>
{{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> --}}
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Checkout </h3>
      <span class="pull-right box-tools"><a href="{{ route('store') }}" class="btn btn-large btn-block btn-primary">Back</a></span>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @foreach ($distributors as $distributor)

        <div class="box-group" id="accordion">
          <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
          <div class="panel box box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $distributor->id }}">
                  Party Name: {{ $distributor->bname }}
                </a>
              </h4>
            </div>
            <div id="collapse{{ $distributor->id }}" class="panel-collapse collapse in">
              <div class="box-body table-responsive">
                <table class="table table-hover " >
                  <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Packing</th>
                      <th>Quantity</th>
                      <th>Scheme</th>
                      <th>GST%</th>
                      <th>MRP</th>
                      <th>Purchase Rate</th>
                      <th>Amount</th>
                      {{-- <th>Rate</th> --}}
                      {{-- <th>Total</th> --}}
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($cartitems as $cartitem)
                    @if ($cartitem->distributor_id == $distributor->id)
                    <?php 
                    $myproduct = $products->find($cartitem->product_id);

                    $gross += $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->purchase_rate * $cartitem->qty;
                    $vat += $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->vat * $cartitem->qty;
                    $net_amount += $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->net_amount * $cartitem->qty;
                    ?>
                    <tr>

                      <td> {{ $myproduct->name   }} {{  !empty($myproduct->brand()->first())? "(".$myproduct->brand()->first()->name.")" :null }}</td>   
                      <td> {{ $myproduct->pack }} </td>             
                      <td> {{ $cartitem->qty }} </td>             
                      <td> {{ $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->scheme }} </td>             
                      <td> {{ $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->tax }} </td>             
                      <td> {{ $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->mrp }} </td>             
                      <td> {{ $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->purchase_rate }} </td>             
                      <td> {{ $myproduct->distributors()->where('distributor_id',$distributor->id)->first()->pivot->net_amount * $cartitem->qty }} </td>             
                      {{-- <td> {{ $myproduct->rate }} </td>              --}}
                      {{-- <td> {{ $cartitem->total }} </td>       --}}
                      <td> <a type="button" class="btn btn-danger item_delete" data-itemid="{{ $cartitem->id }}"><i class="fa fa-fw fa-trash"></i></a> </td>       
                    </tr>
                    @endif

                    @endforeach
                    <table class="table table-striped table-hover">
                      <tbody>
                        <tr>
                          <td width="70%"></td>
                          <td>
                            <span class="pull-left">Gross:</span>
                            <span class="pull-right">{{ $gross }}</span>
                          </td>
                          <td style="width:5%"></td>
                        </tr>
                        <tr>
                          <td width="70%"></td>
                          <td>
                            <span class="pull-left">GST:</span>
                            <span class="pull-right">{{ $vat }}</span>
                          </td>
                          <td style="width:5%"></td>
                        </tr>
                        <tr>
                          <td width="70%"></td>
                          <td>
                            <span class="pull-left">CD:</span>
                            <span class="pull-right">{{ $cd }}</span>
                          </td>
                          <td style="width:5%"></td>
                        </tr>
                        <tr>
                          <td width="70%"></td>
                          <td>
                            <span class="pull-left">Net Amount:</span>
                            <span class="pull-right">{{ $net_amount }}</span>
                          </td>
                          <td style="width:5%"></td>
                        </tr>
                      </tbody>
                    </table>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        {!! Form::open(['method' => 'POST', 'route' => 'complete', 'class' => 'form-horizontal']) !!}
        {!! Form::hidden('checkout_id', $cart->id) !!}
        {!! Form::submit("CONFIRM", ['class' => 'btn btn-lg btn-block btn-success']) !!}
        {!! Form::close() !!}
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</div>
{{-- </div> --}}
@endsection
@section('foot')
<script type="text/javascript">
$(document).ready(function() {

  $(document).on('click','.item_delete', function(event) {
    event.preventDefault();
    item_row = $(this).parent().parent();
    item_id = $(this).data('itemid');
    console.log(item_id);
    $.ajax({
      type: 'DELETE',
      data: {
        _token: "{{ csrf_token() }}",
        _method: 'DELETE',
        id: item_id,
      },
      url: "cart/"+item_id,
    })
    .always(function(data) {
      console.log(data);
      console.log();
      item_row.fadeOut(300, function(){ $(this).remove();});
    });
  });

});
</script>  
@stop