@extends('layouts.public.master')
@section('page_title')
@if (!empty($subcategory))
{{ $subcategory->name }}
@else
Welcome, {{ $user->fname ." ". $user->lname }}
<br>
<p class="text-center label-warning col-xs-12 col-sm-12 col-md-12 col-lg-12" style="font-family: 'Source Sans Pro',sans-serif;">DISCLAIMER: MRP &amp; Purchase Rate may vary on a daily basis. Tax rates may be incorrect. Final price on current bill may differ from actual payable amount. E&amp;OE </p>
{{-- <span class="pull-right"><a href="#" class="btn btn-large btn-warning">Valid Till: {{ $user->package_expiry->format('d/m/Y') }}</a></span> --}}
<div class="clearfix">

</div>
@endif
@stop
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Add Items</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::open(['method' => 'POST', 'route' => 'cart.store', 'class' => 'form-horizontal', 'files' => true]) !!}
<div class="row">
  
        <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
              {!! Form::label('product_id', 'Product Name') !!}
              {!! Form::text('product_id', null, ['id' => 'getproduct','class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('product_id') }}</small>
            </div>
          </div>
        </div>
        <div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <div class="form-group{{ $errors->has('distributor_id') ? ' has-error' : '' }}">
             {!! Form::label('distributor_id', 'Distributor Name') !!}
             {!! Form::text('distributor_id', null, ['id' => 'getdistributor', 'class' => 'form-control', 'required' => 'required']) !!}
             <small class="text-danger">{{ $errors->first('distributor_id') }}</small>
           </div>
         </div>
       </div>
       <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
        <div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
          {!! Form::label('qty', 'Quantity') !!}
          {!! Form::text('qty', null, ['id' => 'addqty','class' => 'form-control', 'required' => 'required']) !!}
          <small class="text-danger">{{ $errors->first('qty') }}</small>
        </div>
      </div>
      <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
        <label>Add to Order</label>
        {!! Form::submit("Add", ['class' => 'btn btn-success btn-block addtocart']) !!}
      </div>
</div>

      {!! Form::close() !!}
    </div>
  </div>
  <!-- /.box-body -->
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Present Order List </h3>
      <div class="box-tools pull-right" id="cart-total" style="font-size: 1.5em; font-weight: bold;">{{  (!empty($cart)) ? $cart->total : 0  }}</div> 
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive">
        <table class="table table-hover " id="carttable">
          <thead>
            <tr>
              <th>Distributor Name</th>
              <th>Product Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($mycartitems as $cartitem)
            <tr>
              <td> {{ $cartitem['bname']}} </td>
              <td> {{ $cartitem['pname']}} </td>
              <td> {{ $cartitem['qty']}} </td>
              <td> {{ $cartitem['price']}} </td>
              <td> <a type="button" class="btn btn-danger item_delete" data-itemid="{{ $cartitem['id']}}"><i class="fa fa-fw fa-trash"></i></a> </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {!! Form::open(['method' => 'POST', 'route' => 'checkout', 'class' => 'form-horizontal']) !!}
        
        {!! Form::hidden('cart_id', (!empty($cart)) ? $cart->id : NULL,['id' => 'checkoutid']) !!}
        
        {!! Form::submit("Checkout", ['class' => 'btn btn-success btn-block btn-lg']) !!}
        
        {!! Form::close() !!}
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</div>
@endsection
@section('foot')
<script type="text/javascript">
$(document).ready(function() {
  var mytable = $('#carttable').DataTable();
  $( "#getproduct" ).autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{ route('getproducts2') }}",
        data: {
          term : request.term,
        },
        success: function(data) {
          console.log(data)
          response(data);
        }
      });
    },
    minLength: 3,
    select: function(event, ui) {
      // event.preventDefault();
      console.log(ui);
      $('#getproduct').data('product_id',ui.item.id);
      console.log($('#getproduct').val());
    }
  });
var closing = false;
  $( "#getdistributor" ).autocomplete({
    source: function(request, response) {
      $.ajax({
        url: "{{ route('getdistributors') }}",
        data: {
          term : request.term,
          product_id : $('#getproduct').data('product_id')
        },
        success: function(data) {
          console.log(data)
          response(data);
        }
      });
    },
    minLength: 0,
    open: function(){
      closing=true;
    },
    close: function(){
      closing = false;
    },
    select: function(event, ui) {
      // event.preventDefault();
      console.log(ui);
      $('#getdistributor').data('distributor_id',ui.item.id);
      $('#getdistributor').data('stock',ui.item.stock);
      console.log($('#getdistributor').val());
      console.log($('#getdistributor').data('stock'));
    }
  }).focus(function () {
     // && ($(this).val() !== "")
   if ((!closing)){
    $(this).autocomplete("search");
  }
});


  $('.addtocart').on('click', function(event) {
    event.preventDefault();
    console.log($(this).parent().parent().find("input#addqty").val())
    qty = $(this).parent().parent().find("input#addqty").val();
    distributor = $(this).parent().parent().find("input#getdistributor").data('distributor_id');
    stock = $(this).parent().parent().find("input#getdistributor").data('stock');
    product = $(this).parent().parent().find("input#getproduct").data('product_id');
    console.log(qty,distributor,product,stock)
if (qty <= stock) {
  if (qty && distributor && product) {
    $.ajax({
      url: '{{ route("cart.store") }}',
      type: 'POST',
      data: {
        _token : "{{ csrf_token() }}",
        qty: qty,
        distributor_id: distributor,
        product_id: product,
      },
    })
    .done(function(data) {
      console.log(data);
      $("input#addqty").val('');
      $("input#getdistributor").val('');
      $("input#getproduct").val('');
      $("input#getdistributor").data('distributor_id','');
      $("input#getproduct").data('product_id','');
      $('#cart-total').html(data.total);
      $('#checkoutid').val(data.cartid);
      mytable.row.add([ data.bname, data.pname, data.qty, data.price, '<a type="button" class="btn btn-danger item_delete" data-itemid="'+data.id+'"><i class="fa fa-fw fa-trash"></i></a>']).draw();
      // $('#cart_menu').append('<li><a href="#"><div class="pull-left"><img src="'+data.image+'" class="img-circle" alt=""></div><h4>'+data.name+'<small>'+data.qty+' nos.</small></h4><p>'+data.desc+'</p><p class="pull-right">Rs.'+data.price+'</p></a></li>');
      // $('.navcart_count').html(data.count);
    });
  } else{
    alert("Please fill all fields");

  };
} else{

  alert("Insufficient stock ! \n Please reduce quantity!");
  $("input#addqty").val('');
};
  });
  $(document).on('click','.item_delete', function(event) {
    event.preventDefault();
    item_row = $(this).parent().parent();
    item_id = $(this).data('itemid');
    console.log(item_id);
    $.ajax({
      type: 'DELETE',
      data: {
        _token: "{{ csrf_token() }}",
        _method: 'DELETE',
        id: item_id,
      },
      url: "cart/"+item_id,
    })
    .always(function(data) {
      console.log(data);
      console.log();
      item_row.fadeOut(300, function(){ $(this).remove();});
    });
  });
});
</script>
@stop
