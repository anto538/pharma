@extends('layouts.public.master')
@section('content')
{{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> --}}
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Your Cart Summary </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Product</th>
              <th>Rate</th>
              <th>Qty</th>
              <th>Total</th>
              <th >Actions</th>
            </tr>
          </thead>
          <tbody>
           @foreach ($cartitems as $cartitem)
           <?php 
           $product = $products->find($cartitem->product_id);
           $tax = $taxes->find($product->branches()->find($cartitem->branch_id)->pivot->tax_id);
           if (!empty($product_images->where('prod_id',$cartitem->product_id))) {
            $image = $product_images->where('prod_id',$cartitem->product_id);
          } else {
            $image = null;
          } 
          ?> 
          <tr>
            <td>
              <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <span class="info-box-icon " style="text-align: center; position: relative; background-color: #fff; border: 1px solid #3c8dbc">
                  <img src="{{ (!$image->isEmpty()) ? ( URL::to($image->first()->image_path)) : "" }}" alt="" class="img-responsive" style="max-width:100%;
                  max-height:100%;
                  vertical-align: middle; position: absolute; top: 50%;
                  left: 50%; transform: translate(-50%, -50%);" />
                </span>
              </div>
              <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
               <span class="info-box-number">{{ $product->name }}</span>
               <b>Category</b>: {{ $product->subcategories->first()->name }} of {{ $categories->find($product->subcategories->first()->id)->name }}
               <br>
               <b>Warranty</b>: {{ $product->warranty }} | 
               <b>Tax</b>: <span class="item_tax">{{ $cartitem->tax }}</span> ({{ $tax->name}} @  {{ $tax->rate}}%)
             </div>
           </div>
         </td>
         <td class="item_rate"> {{ $cartitem->rate }}</td>
         <td>  
          {!! Form::selectRange('qty', 1, 100, $cartitem->qty, ['class' => 'text-center select2 item_qty', 'required' => 'required', 'data-itemid' => $cartitem->id, 'style' =>'width: 80px;']) !!}
        </td>
        <td class="item_total"> {{ $cartitem->total }}</td>
        <td>
          <a  class="btn btn-large  btn-warning item_edit" data-itemid="{{ $cartitem->id }}"><i class="fa fa-fw fa-pencil"></i></a>
          <a  class="btn btn-large  btn-danger item_delete" data-itemid="{{ $cartitem->id }}"><i class="fa fa-fw fa-trash"></i></a>
        </td>
        
      </tr>
      @endforeach
      <tr>
        <td colspan="3"></td>
        <td><h3>Tax :</h3></td>
        <td><h3><span id="cart_tax">{{ $cart->tax }}</span></h3></td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td><h3>Shipping :</h3></td>
        <td><h3><span id="cart_shipping">{{ $cart->shipping }}</span></h3></td>
      </tr>
      <tr>
        <td colspan="3"></td>
        <td><h3><b>Total :</b></h3></td>
        <td><h3><b><span id="cart_total">{{ $cart->total }}</span></b></h3></td>
      </tr>
    </tbody>
  </table>

</div>
</div>
<!-- /.box-body -->
</div>
</div>
{{-- </div> --}}
@endsection
@section('foot')
<script type="text/javascript">
$(document).ready(function() {
  $('.item_edit').on('click', function(event) {
    event.preventDefault();
    item_row = $(this).parent().parent();
    item_id = $(this).data('itemid');
    qty = $(this).parent().parent().find('.item_qty').val();

    console.log($(this).data('itemid'));
    console.log(item_id);
    console.log($(this).parent().parent().find('.item_qty').val());
    $.ajax({
      url: "{{ route('cart.update',"+item_id+") }}",
      type: 'PATCH',
      data: {
        _token: "{{ csrf_token() }}",
        item_id: item_id,
        qty: qty,
      },
    })
    .success(function(data) {
      console.log(data);
      console.log(item_row.find('.item_tax'));
      item_row.find('.item_tax').html(data.tax);
      item_row.find('.item_total').html(data.total);
      $('#cart_tax').html(data.cart_tax);
      $('#cart_shipping').html(data.cart_shipping);
      $('#cart_total').html(data.cart_total);
    });

  });
  $('.item_delete').on('click', function(event) {
    event.preventDefault();
    item_row = $(this).parent().parent();
    item_id = $(this).data('itemid');
    console.log(item_id);
    $.ajax({
      type: 'DELETE',
      data: {
        _token: "{{ csrf_token() }}",
        _method: 'DELETE',
        id: item_id,
      },
      url: "cart/"+item_id,
    })
    .always(function(data) {
      console.log(data);
      console.log();
      item_row.fadeOut(300, function(){ $(this).remove();});
    });

  });


});
</script>
@stop