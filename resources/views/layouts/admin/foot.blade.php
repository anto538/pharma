<!-- jQuery 2.2.3 -->
<script src="{{ URL::to('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ URL::to('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> --}}
{{-- <script src="{{ URL::to('theme/plugins/morris/morris.min.js') }}"></script> --}}
<!-- Sparkline -->
{{-- <script src="{{ URL::to('theme/plugins/sparkline/jquery.sparkline.min.js') }}"></script> --}}
<!-- jvectormap -->
{{-- <script src="{{ URL::to('theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script> --}}
{{-- <script src="{{ URL::to('theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> --}}
<!-- jQuery Knob Chart -->
{{-- <script src="{{ URL::to('theme/plugins/knob/jquery.knob.js') }}"></script> --}}
<!-- daterangepicker -->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> --}}
{{-- <script src="{{ URL::to('theme/plugins/daterangepicker/daterangepicker.js') }}"></script> --}}
<!-- datepicker -->
<script src="{{ URL::to('theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
{{-- <script src="{{ URL::to('theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script> --}}
<!-- Slimscroll -->
{{-- <script src="{{ URL::to('theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script> --}}
<!-- FastClick -->
{{-- <script src="{{ URL::to('theme/plugins/fastclick/fastclick.js') }}"></script> --}}
<!-- DATA TABLE -->
<script src="{{ URL::to('theme/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('theme/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::to('theme/dist/js/app.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{ URL::to('theme/dist/js/pages/dashboard.js') }}"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="dist/js/demo.js"></script> --}}
<script src="//cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>
<script type="text/javascript" src="http://cdn.ckeditor.com/4.5.10/full/lang/en.js"></script>
<script type="text/javascript" src="http://cdn.ckeditor.com/4.5.10/full/styles.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
{{-- <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script> --}}
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
{{-- <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script> --}}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{-- <script src="{{ URL::to('file_uploader/js/jquery.iframe-transport.js') }}"></script> --}}
<!-- The basic File Upload plugin -->
{{-- <script src="{{ URL::to('file_uploader/js/jquery.fileupload.js') }}"></script> --}}
<!-- The File Upload processing plugin -->
{{-- <script src="{{ URL::to('file_uploader/js/jquery.fileupload-process.js') }}"></script> --}}
<!-- The File Upload image preview & resize plugin -->
{{-- <script src="{{ URL::to('file_uploader/js/jquery.fileupload-image.js') }}"></script> --}}

<!-- The File Upload validation plugin -->
{{-- <script src="{{ URL::to('file_uploader/js/jquery.fileupload-validate.js') }}"></script> --}}
<link href="{{ URL::to('dropzone/basic.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::to('dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
<script src="{{ URL::to('dropzone/dropzone.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
	CKEDITOR.editorConfig = function( config ) {
		config.language = 'en';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;
	};

});
</script>
<script>
$(document).ready(function(){
	$('.dataTable').DataTable();
});
</script>

@yield('foot')