          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa  fa-shopping-cart"></i>
              @if (!empty($cartitems))
              <span class="label label-success navcart_count">{{ $cartitems->count() }}</span>
              @endif
              
            </a>
            <ul class="dropdown-menu">
              {{-- <li class="header">You have <span class="navcart_count">{{ $cartitems->count() OR 0 }}</span> items in your cart</li> --}}
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu" id="cart_menu">
                  @if (!empty($cartitems))
                  @foreach ($cartitems as $cartitem)
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="{{ (!$image->isEmpty()) ? ( URL::to($image->first()->image_path)) : "" }}" class="img-circle" alt="">
                      </div>
                      <h4>{{ $product->find($cartitem->product_id)->name }}<small>{{ $cartitem->qty }} nos</small></h4>
                      <p>{{ substr($product->find($cartitem->product_id)->desc, 0, 10) }}...</p><p class="pull-right">Rs. {{ $cartitem->total }}</p>
                    </a>
                  </li>
                  @endforeach
                  @endif
                </ul>
              </li>
              <li class="footer"><a href="{{ route('cart.index') }}">Go to Cart</a></li>
            </ul>
          </li>