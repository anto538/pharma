<!DOCTYPE html>
<html>
<head>
  @include('layouts.distributor.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>P</b>Ti</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Pharma</b>Tieup</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            {{-- @include('layouts.distributor.message_nav') --}}
            <!-- Notifications: style can be found in dropdown.less -->
            {{-- @include('layouts.distributor.notification_nav') --}}
            <!-- Tasks: style can be found in dropdown.less -->
            {{-- @include('layouts.distributor.task_nav') --}}
            <!-- User Account: style can be found in dropdown.less -->
            {{-- @include('layouts.distributor.profile_nav') --}}
            <!-- Control Sidebar Toggle Button -->
 {{--            <li>
              <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li> --}}
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    @include('layouts.distributor.menu')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          @yield('page_title')
          <small>@yield('page_title_small')</small>
        </h1>
        <ol class="breadcrumb">
          @yield('breadcrumbs')
       {{--  <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Dashboard</li> --}}
     </ol>
   </section>

   <!-- Main content -->
   <section class="content">
    <!-- Main row -->
    <div class="row">
      @yield('content')

      

    </div>
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
 @include('layouts.distributor.footer')
</footer>
<!-- Control Sidebar -->
@include('layouts.distributor.rsidebar')
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@include('layouts.distributor.foot')
</body>
</html>
    {{--  <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Block </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
            </div>
            <!-- /.box-body -->
          </div> --}}