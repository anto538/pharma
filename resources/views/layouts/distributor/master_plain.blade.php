<!DOCTYPE html>
<html>
<head>
  @include('layouts.distributor.head')
</head>
<body class="skin-blue  wysihtml5-supported sidebar-collapse ">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>P</b>Ti</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Pharma</b>Tieup</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->


        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

          </ul>
        </div>
      </nav>
    </header>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <!-- Main content -->
      <section class="content">
        <!-- Main row -->
        <div class="row">
         @yield('content')
       </div>
       <!-- /.row (main row) -->
     </section>
     <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
   <footer class="main-footer">
     @include('layouts.distributor.footer')
   </footer>
 </div>
 <!-- ./wrapper -->
 @include('layouts.distributor.foot')
</body>
</html>
{{--  <div class="box box-solid">
<div class="box-header with-border">
  <i class="fa fa-text-width"></i>

  <h3 class="box-title">Block </h3>
</div>
<!-- /.box-header -->
<div class="box-body">

</div>
<!-- /.box-body -->
</div> --}}