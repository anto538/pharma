@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Add Product </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if (!isset($edit))
				{!! Form::open(['method' => 'POST', 'route' => 'distributor.dproduct.store', 'class' => 'form-horizontal']) !!}
				@else
				{!! Form::model($product, ['route' => ['distributor.dproduct.update', $product->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
				@endif
				{{-- {{ dd($distributor) }} --}}
				{!! Form::hidden('product_id', null,['id'=>'prodid']) !!}
				{!! Form::hidden('brand_id', null,['id'=>'brandid']) !!}
				<div class="form-group{{ $errors->has('brand_name') ? ' has-error' : '' }}">
					{!! Form::label('brand_name', 'Brand Name') !!}
					{!! Form::text('brand_name', null, ['class' => 'form-control', 'required' => 'required','id'=>'getbrand']) !!}
					<small class="text-danger">{{ $errors->first('brand_name') }}</small>
				</div>	
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					{!! Form::label('name', 'Product Name (search for a product or type a name for new product)') !!}
					{!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required','id'=>'getproduct']) !!}
					<small class="text-danger">{{ $errors->first('name') }}</small>
				</div>	
				
				<div class="form-group{{ $errors->has('strength') ? ' has-error' : '' }}">
					{!! Form::label('strength', 'Strength') !!}
					{!! Form::text('strength', isset($product)?$product->strength: null, ['class' => 'form-control', 'required' => 'required', 'id' => 'strength']) !!}
					<small class="text-danger">{{ $errors->first('strength') }}</small>
				</div>	
				
				<div class="form-group{{ $errors->has('pack') ? ' has-error' : '' }}">
					{!! Form::label('pack', 'Packing') !!}
					{!! Form::text('pack', isset($product)?$product->pack: null, ['class' => 'form-control', 'required' => 'required','id' => 'pack']) !!}
					<small class="text-danger">{{ $errors->first('pack') }}</small>
				</div>	
				<div class="form-group{{ $errors->has('scheme') ? ' has-error' : '' }}">
				    {!! Form::label('scheme', 'Scheme if Any') !!}
				    {!! Form::text('scheme', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->scheme: null, ['class' => 'form-control']) !!}
				    <small class="text-danger">{{ $errors->first('scheme') }}</small>
				</div>
				<div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
					{!! Form::label('quantity', 'Quantity') !!}
					{!! Form::text('quantity', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->quantity: null, ['class' => 'form-control', 'required' => 'required']) !!}
					<small class="text-danger">{{ $errors->first('quantity') }}</small>
				</div>
				<div class="form-group{{ $errors->has('purchase_rate') ? ' has-error' : '' }}">
					{!! Form::label('purchase_rate', 'Purchase Rate') !!}
					{!! Form::text('purchase_rate', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->purchase_rate: null, ['class' => 'form-control', 'required' => 'required','id'=>'purchase_rate']) !!}
					<small class="text-danger">{{ $errors->first('purchase_rate') }}</small>
				</div>
				<div class="form-group{{ $errors->has('mrp') ? ' has-error' : '' }}">
					{!! Form::label('mrp', 'MRP') !!}
					{!! Form::text('mrp', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->mrp: null, ['class' => 'form-control', 'required' => 'required','id'=>'mrp']) !!}
					<small class="text-danger">{{ $errors->first('mrp') }}</small>
				</div>
				<div class="form-group{{ $errors->has('tax') ? ' has-error' : '' }}">
					{!! Form::label('tax', 'Tax Rate') !!}
					{!! Form::select('tax',["5"=>"5 %","10"=>"10 %"], isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->tax: null, ['id' => 'tax', 'class' => 'form-control', 'required' => 'required','id'=>'taxrate']) !!}
					<small class="text-danger">{{ $errors->first('tax') }}</small>
				</div>
				<div class="form-group{{ $errors->has('vat') ? ' has-error' : '' }}">
					{!! Form::label('vat', 'VAT') !!}
					{!! Form::text('vat1', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->vat: null, ['class' => 'form-control', 'required' => 'required','id'=>'vat', "disabled" => "disabled"]) !!}
					<small class="text-danger">{{ $errors->first('vat') }}</small>
				</div>
				<div class="form-group{{ $errors->has('cash_discount') ? ' has-error' : '' }}">
					{!! Form::label('cash_discount', 'Cash Discount (CD)') !!}
					{!! Form::text('cash_discount', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->cash_discount: null, ['class' => 'form-control','id'=>'cash_discount']) !!}
					<small class="text-danger">{{ $errors->first('cash_discount') }}</small>
				</div>
				<div class="form-group{{ $errors->has('net_amount') ? ' has-error' : '' }}">
					{!! Form::label('net_amount', 'Net Amount') !!}
					{!! Form::text('net_amount1', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->net_amount: null, ['class' => 'form-control', 'required' => 'required', "disabled" => "disabled", "id" => "net_amount"]) !!}
					<small class="text-danger">{{ $errors->first('net_amount') }}</small>
				</div>
				{!! Form::hidden('vat', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->vat: null,['id'=>"vat2"]) !!}
				{!! Form::hidden('net_amount', isset($distributor)?$distributor->products()->where('product_id',$product->id)->first()->pivot->net_amount: null,['id'=>"net_amount2"]) !!}
				<div class="btn-group pull-right">
					{!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
				</div>
				
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection
@section('foot')
<script>
$(document).ready(function() {
	bid = $('#brandid').val();

	$("#getbrand").on('keyup', function(event) {
		if ($(this).val() == "") {
			bid = null;
			$('#brandid').val("");
		} 
	});
	$("#getbrand").autocomplete({
		source: function(request, response) {
			$.ajax({
				url: "{{ route('getbrands') }}",
				data: {
					term : request.term,
				},
				success: function(data) {
					response(data);
					console.log(data)
				}
			});
		},
		minLength: 2,
		select: function(event, ui) {
			console.log(ui);
			$('#getbrand').data('brand_id',ui.item.id);
			$('#brandid').val(ui.item.id);
			bid = ui.item.id;
			console.log(bid);
			console.log($('#getbrand').val());
		}
	});
	$("#getproduct").autocomplete({
		source: function(request, response) {
		if ( bid =="" ) {  bid = null };
			$.ajax({
				url: "{{ route('getproducts') }}",
				data: {
					term : request.term,
					bid : $('#brandid').val()
				},
				success: function(data) {
					console.log(bid);
					response(data);
					console.log(data)
				}
			});
		},
		minLength: 3,
		select: function(event, ui) {
			console.log(ui);
			$('#getproduct').data('product_id',ui.item.id);
			$('#prodid').val(ui.item.id);
			$('#strength').val(ui.item.strength);
			$('#pack').val(ui.item.pack);
			$('#strength').attr("disabled","disabled");
			$('#pack').attr("disabled","disabled");
			console.log($('#getproduct').val());
		}
	});
	var purchase_rate = 0;
	var mrp = 0;
	var vat = 0;
	var cash_discount = 0;
	var net_amount = 0;
	var tax = 0;
	if ($('#cash_discount').val() == "") {
		$('#cash_discount').val(0)
	};
	$('#purchase_rate').on('keyup', function(event) {
		event.preventDefault();
		purchase_rate = $(this).val();
		getTotal();
	});
	$('#mrp').on('keyup', function(event) {
		event.preventDefault();
		mrp = $(this).val();
		getTotal();
	});
	$('#vat').on('keyup', function(event) {
		event.preventDefault();
		vat = $(this).val();
		getTotal();
	});
	$('#cash_discount').on('keyup', function(event) {
		event.preventDefault();
		setTimeout(function  () {
			if ($('#cash_discount').val() == "") {
				$('#cash_discount').val(0)
				cash_discount = $('#cash_discount').val(0)
			}
			getTotal();	
		},1000);

		cash_discount = $(this).val();
		getTotal();
	});
	$('#net_amount').on('keyup', function(event) {
		event.preventDefault();
		net_amount = $(this).val();
		getTotal();
	});
	$('#taxrate').on('change', function(event) {
		event.preventDefault();
		tax = $(this).val();
		getTotal();
	});
	function getTotal () {

		tax =  $('#taxrate').val();
		vat = purchase_rate * tax / 100;
		$('#vat').val(vat);
		$('#vat2').val(vat);
		net_amount = ( parseFloat(purchase_rate) + parseFloat(vat) ) - parseFloat(cash_discount);
		$('#net_amount').val(net_amount);
		$('#net_amount2').val(net_amount);
		// console.log(purchase_rate);
		// console.log(mrp);
		// console.log(vat);
		// console.log(cash_discount);
		// console.log(net_amount);
		// console.log(tax);
	}
});
</script>
@stop