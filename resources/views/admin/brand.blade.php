@extends('layouts.admin.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Register With Us </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if (!isset($edit))
				{!! Form::open(['method' => 'POST', 'route' => 'admin.mbrand.store', 'class' => 'form-horizontal']) !!}
				@else
				{!! Form::model($brand, ['route' => ['admin.mbrand.update', $brand->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
				@endif
				
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				    {!! Form::label('name', 'Brand Name') !!}
				    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
				    <small class="text-danger">{{ $errors->first('name') }}</small>
				</div>	
				<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
				    {!! Form::label('desc', 'Description') !!}
				    {!! Form::textarea('desc', null, ['class' => 'form-control ckeditor']) !!}
				    <small class="text-danger">{{ $errors->first('desc') }}</small>
				</div>
				<div class="btn-group pull-right">
					{!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
				</div>
				
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection