@extends('layouts.admin.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Register With Us </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table class="table table-hover table-responsive datatable">
					<thead>
						<tr>
							<th>Name</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($brands as $brand)
						<tr>
							<td>{{ $brand->name }}</td>
							<td><a href="{{ route('admin.mbrand.edit',$brand->id) }}" class="btn btn-warning">EDIT</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection