@extends('layouts.admin.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Register With Us </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if (!isset($edit))
				{!! Form::open(['method' => 'POST', 'route' => 'admin.mproduct.store', 'class' => 'form-horizontal']) !!}
				@else
				{!! Form::model($product, ['route' => ['admin.mproduct.update', $product->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
				@endif
				
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					{!! Form::label('name', 'Product Name') !!}
					{!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
					<small class="text-danger">{{ $errors->first('name') }}</small>
				</div>	
				
				<div class="form-group{{ $errors->has('strength') ? ' has-error' : '' }}">
					{!! Form::label('strength', 'Strength') !!}
					{!! Form::text('strength', null, ['class' => 'form-control', 'required' => 'required']) !!}
					<small class="text-danger">{{ $errors->first('strength') }}</small>
				</div>	
				
				<div class="form-group{{ $errors->has('pack') ? ' has-error' : '' }}">
					{!! Form::label('pack', 'Pack / Scheme') !!}
					{!! Form::text('pack', null, ['class' => 'form-control', 'required' => 'required']) !!}
					<small class="text-danger">{{ $errors->first('pack') }}</small>
				</div>	
{{-- 				<div class="form-group{{ $errors->has('brand_id') ? ' has-error' : '' }}">
					{!! Form::label('brand_id', 'Select Brand / Company') !!}
					{!! Form::select('brand_id',$brands->lists('name','id'), null, ['id' => 'brand_id', 'class' => 'form-control', 'required' => 'required']) !!}
					<small class="text-danger">{{ $errors->first('brand_id') }}</small>
				</div> --}}
				<div class="btn-group pull-right">
					{!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
				</div>
				
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection