@extends('layouts.admin.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Manage Clients </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-hover table-responsive dataTable">
       <thead>
         <tr>
           <th>Retailer Name</th>
           <th>Customer Name</th>
           <th>Email Id</th>
           <th>Actions</th>
         </tr>
       </thead>
       <tbody>
        @foreach ($users as $user)
        <tr>
         <td>{{ $user->bname }}</td>
         <td>{{ $user->fname . ' ' . $user->lname }}</td>
         <td>{{ $user->bemail }}</td>
         <td><a href="{{ route('admin.muser.edit',$user->id) }}" class="btn btn-warning">EDIT</a><a href="{{ route('admin.muser.destroy',$user->id) }}" class="btn btn-warning">Delete</a></td>
       </tr>
       @endforeach
     </tbody>
   </table>
 </div>
 <!-- /.box-body -->
</div>
</div>
@endsection