@extends('layouts.admin.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Add Distributor to Product : {{ $product->name }} </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				{!! Form::open(['method' => 'POST', 'route' => 'admin.mproduct.store', 'class' => 'form-horizontal', 'files' => true]) !!}

				{!! Form::hidden('product_id', $product->id,['id' => 'product_id','data-id' =>  $product->id]) !!}
				<table class="table table-hover dataTable">
					<thead>
						<tr>
							<th>Distributor Name</th>
							<th>Select</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($distributors as $distributor)
						<tr>
							<td> {{ $distributor->bname }} </td>
							<td>
								<div class="form-group">
									<div class="checkbox{{ $errors->has('selectprod') ? ' has-error' : '' }}">
										<label for="selectprod">
											{!! Form::checkbox('selectprod', $distributor->id, (!empty($distributor->products()->first())) ? ( !empty($distributor->find(1)->products()->find($product->id)->first()) ? 'checked' : null ) : null, ['class' => 'selectprod']) !!} 
										</label>
									</div>
									<small class="text-danger">{{ $errors->first('selectprod') }}</small>
								</div>
								<span class="action"></span>								
							</td>
						</tr>
						@endforeach	
					</tbody>
				</table>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection
@section('foot')
<script type="text/javascript">
$(document).ready(function() {
	$('input.selectprod').on('click', function(event) {
		var product_id = $('#product_id').val();
		var id = $(this).val();
		var toggle = $(this).prop("checked") ? 1 : 0;
		console.log(id);
		$.ajax({
			url: "{{ route('toggledistributor') }}",
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				id : id,
				toggle : toggle,
				product_id : product_id
			},
		})
		.always(function(data) {
			console.log(data);
		});

		
		
	});;
});
</script>
@stop