  <aside class="main-sidebar">
  	<!-- sidebar: style can be found in sidebar.less -->
  	<section class="sidebar">
  		<!-- Sidebar user panel -->
{{--   		<div class="user-panel">
  			<div class="pull-left image">
  				<img src="{{ URL::to('theme/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
  			</div>
  			<div class="pull-left info">
  				<p>Admin</p>
  				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
  			</div>
  		</div> --}}
  		<!-- search form -->
{{--   		<form action="#" method="get" class="sidebar-form">
  			<div class="input-group">
  				<input type="text" name="q" class="form-control" placeholder="Search...">
  				<span class="input-group-btn">
  					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
  					</button>
  				</span>
  			</div>
  		</form> --}}
    
  		<!-- /.search form -->
  		<!-- sidebar menu: : style can be found in sidebar.less -->
  		<ul class="sidebar-menu">
  			<li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>User Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('admin.muser.create') }}"><i class="fa fa-circle-o"></i> Create User</a></li>
            <li><a href="{{ route('admin.muser.index') }}"><i class="fa fa-circle-o"></i> Manage User</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Distributor Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('admin.mdistributor.create') }}"><i class="fa fa-circle-o"></i> Create Distributor</a></li>
            <li><a href="{{ route('admin.mdistributor.index') }}"><i class="fa fa-circle-o"></i> Manage Distributor</a></li>
          </ul>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Product Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('admin.mproduct.create') }}"><i class="fa fa-circle-o"></i> Create Product</a></li>
            <li><a href="{{ route('admin.mproduct.index') }}"><i class="fa fa-circle-o"></i> Manage Product</a></li>
          </ul>
        </li>
<li><a href="{{ route('adminlogout') }}"><i class="fa fa-book"></i> <span>Logout</span></a></li>
        
        {{-- 
        TEMPLATE

        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href=""><i class="fa fa-circle-o"></i> Dashboard             <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span></a></li>
            
          </ul>
        </li>


 --}}
        {{-- Multilevel --}}
{{--   			<li class="treeview">
  				<a href="#">
  					<i class="fa fa-share"></i> <span>Multilevel</span>
  					<span class="pull-right-container">
  						<i class="fa fa-angle-left pull-right"></i>
  					</span>
  				</a>
  				<ul class="treeview-menu">
  					<li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
  					<li>
  						<a href="#"><i class="fa fa-circle-o"></i> Level One
  							<span class="pull-right-container">
  								<i class="fa fa-angle-left pull-right"></i>
  							</span>
  						</a>
  						<ul class="treeview-menu">
  							<li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
  							<li>
  								<a href="#"><i class="fa fa-circle-o"></i> Level Two
  									<span class="pull-right-container">
  										<i class="fa fa-angle-left pull-right"></i>
  									</span>
  								</a>
  								<ul class="treeview-menu">
  									<li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
  									<li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
  								</ul>
  							</li>
  						</ul>
  					</li>
  					<li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
  				</ul>
  			</li> --}}
        {{-- Normal Menu Link --}}
{{--   			<li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li> --}}
{{--   			<li class="header">LABELS</li>
  			<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
  			<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
  			<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}
  		</ul>
  	</section>
  	<!-- /.sidebar -->
  </aside>


