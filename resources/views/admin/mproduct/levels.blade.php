
@foreach ($branches as $branch)
{!! Form::open(['method' => 'POST', 'route' => 'branchlevelprices', 'class' => 'form-horizontal prices']) !!}

<div class="box box-primary box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">{{ $branch->name }}</h3>
		{!! Form::hidden('branch_id', $branch->id) !!}
		{!! Form::hidden('product_id', $product->id) !!}
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
			</button>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Level Name</th>
					<th>Level Price</th>
					<th>Level Rewards</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($levels as $level)
				<tr>
					<td>{{ $level->name }} {!! Form::hidden('level_id[]', $level->id) !!}</td>
					<td>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
								{!! Form::text('price[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
								<small class="text-danger">{{ $errors->first('price') }}</small>
							</div>
						</div>
					</td>
					<td>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group{{ $errors->has('rewards') ? ' has-error' : '' }}">
								{!! Form::text('rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
								<small class="text-danger">{{ $errors->first('rewards') }}</small>
							</div>
						</div>
					</td>
				</tr>					
				@endforeach
			</tbody>
		</table>
		<div class="btn-group pull-right">
			{!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
		</div>

		{!! Form::close() !!}
	</div>
	<!-- /.box-body -->
</div>

@endforeach

{{-- <table class="table table-hover">
	<thead>
		<tr>
			<th>Level Name</th>
			<th>Level Price</th>
			<th>Level Reward Points</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($levels as $level)
		<tr>
			<td>{{ $level->name }} {!! Form::hidden('level_id[]', $level->id) !!}</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
						{!! Form::text('price[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('price') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('rewards') ? ' has-error' : '' }}">
						{!! Form::text('rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('rewards') }}</small>
					</div>
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table> --}}