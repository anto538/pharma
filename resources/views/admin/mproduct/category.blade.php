@foreach ($categories as $category)
<ul>
	<li>{{ $category->name }}

		@foreach ($subcategories->where('category_id',$category->id) as $subcategory)
		{{-- {{ dd($product->subcategories()->find($subcategory->id)) }} --}}
		<div class="form-group">
			<div class="checkbox{{ $errors->has('sub_category_id') ? ' has-error' : '' }}">
				<label for="sub_category_id">
					{!! Form::checkbox('sub_category_id[]', $subcategory->id, 
						((isset($edit)) ? ((!empty($product->subcategories()->find($subcategory->id))) ? TRUE : NULL ): NULL)
						, ['id' => 'sub_category_id']) !!} {{ $subcategory->name }}
					</label>
				</div>
				<small class="text-danger">{{ $errors->first('sub_category_id') }}</small>
			</div>

			@endforeach
		</li>

	</ul>
	@endforeach