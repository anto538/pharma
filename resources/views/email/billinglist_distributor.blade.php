<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Order By {{ $user->bname }}</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<center>
		<img src="{{ URL::to('logo.png') }}" alt="" style="width: 200px; height: auto">
	</center>
	<table border="1" style="text-align: center" cellpadding="10px" cellspacing="0px">
		<caption>List of Products</caption>
		<thead>
			<tr>
				<th>Product Name</th>
				<th>Product Qty</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($products as $product)
			<tr>
				<td>{{ $product->name }}</td>
				<td>{{ $cartitems->where('product_id',$product->id)->sum('qty') }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
<h2>Customer Details</h2>
<b>Address:</b> <br>
{{ $user->baddr1 }},<br>
{{ $user->baddr2 }},<br>
{{ $user->baddr3 }},<br>
{{ $user->baddr4 }},<br>
{{ $user->bcity }},<br>
{{ $user->bstate }},<br>
{{ $user->bcountry }}<br>
<b>Contact:</b> <br>
{{ $user->bphone1 }},<br>
{{ $user->bphone2 }},<br>
{{ $user->bphone3 }},<br>
{{ $user->bmobile }},<br>
{{ $user->bfax }},<br>
{{ $user->bemail }},<br>
</body>
</html>