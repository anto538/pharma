<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Order By {{ $user->bname }}</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<center>
		<img src="{{ URL::to('logo.png') }}" alt="" style="width: 200px; height: auto">
	</center>
	@foreach ($distributors as $distributor)
	<table  border="1" style="text-align: center" cellpadding="10px" cellspacing="0px">
		<thead>
			<tr>
				<td colspan="7"><h2>Distributor Details</h2></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="2">
					<b>Name: {{ $distributor->bname }}</b> <br>
					<b>Address:</b> <br>
					{{ $distributor->baddr1 }},<br>
					{{ $distributor->baddr2 }},<br>
					{{ $distributor->baddr3 }},<br>
					{{ $distributor->baddr4 }},<br>
					{{ $distributor->bcity }},<br>
					{{ $distributor->bstate }},<br>
					{{ $distributor->bcountry }}<br>
				</td>
				<td colspan="3">
					<b>Contact:</b> <br>
					{{ $distributor->bphone1 }},<br>
					{{ $distributor->bphone2 }},<br>
					{{ $distributor->bphone3 }},<br>
					{{ $distributor->bmobile }},<br>
					{{ $distributor->bfax }},<br>
					{{ $distributor->bemail }}<br><br>
				</td>
				<td colspan="2">
					<b>Bank Details:</b> <br>
					{{ $distributor->bank_name }},<br>
					{{ $distributor->bank_accname }},<br>
					{{ $distributor->bank_type }},<br>
					{{ $distributor->bank_branch }},<br>
					{{ $distributor->bank_ifsc }},<br>
				</td>
			</tr>
			<tr>
				<td colspan="7"><h2>Bill Details</h2></td>
			</tr>
			<tr>
				<th>Product Name</th>
				<th>Rate w/ Tax</th>
				<th>Scheme</th>
				<th>Tax Rate</th>
				<th>Tax Amount</th>
				<th>Quantity</th>
				<th>Total</th>

			</tr>
			@foreach ($cartitems->where('distributor_id',$distributor->id) as $cartitem)
			<?php 
			$myproduct = $products->find($cartitem->product_id);
			$pivot = $cartitem->distributor()->first()->products()->where('product_id',$cartitem->product_id)->first()->pivot;
			?>
			<tr>
				<td> {{ $myproduct->name . ' - ' . $myproduct->strength . ' - ' . $myproduct->pack }} </td>   
				<td> {{ $cartitem->rate }} </td>             
				<td> {{ $pivot->scheme }} </td>             
				<td> {{ $pivot->tax }} %</td>             
				<td> {{ $pivot->vat }} </td>             
				<td> {{ $cartitem->qty }} </td>             
				<td> {{ $cartitem->total }} </td>             
			</tr>
			@endforeach
			<tr>
				<td colspan="4" style="text-align: center;font-weight: bold">Tax</td>
				<td colspan="3" style="text-align: right;font-weight: bold">{{ $cartitems->sum("tax") }}</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;font-weight: bold">Gross Total w/ Tax</td>
				<td colspan="3" style="text-align: right;font-weight: bold">{{ $cartitems->sum("rate") }}</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: center;font-weight: bold">GrandTotal</td>
				<td colspan="3" style="text-align: right;font-weight: bold">{{ $cartitems->sum("total") }}</td>
			</tr>
		</tbody>
	</table>
		<hr>
	@endforeach


</body>
</html>