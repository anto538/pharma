<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Order By {{ $user->bname }}</title>
	<link rel="stylesheet" href="">
</head>
<body>
		<center>
		<img src="{{ URL::to('logo.png') }}" alt="" style="width: 200px; height: auto">
	</center>
<h2>Customer Details</h2>
<b>Address:</b> <br>
{{ $user->baddr1 }},<br>
{{ $user->baddr2 }},<br>
{{ $user->baddr3 }},<br>
{{ $user->baddr4 }},<br>
{{ $user->bcity }},<br>
{{ $user->bstate }},<br>
{{ $user->bcountry }}<br>
<b>Contact:</b> <br>
{{ $user->bphone1 }},<br>
{{ $user->bphone2 }},<br>
{{ $user->bphone3 }},<br>
{{ $user->bmobile }},<br>
{{ $user->bfax }},<br>
{{ $user->bemail }},<br>
	@foreach ($distributors as $distributor)
	<hr>
	<b>{{ $distributor->bname }}</b>
	<table  border="1" style="text-align: center" cellpadding="10px" cellspacing="0px">
		<thead>
			<tr>
				<th>Product Name</th>
				<th>Quantity</th>

			</tr>
		</thead>
		<tbody>
			@foreach ($cartitems as $cartitem)
			<?php 
			$myproduct = $products->find($cartitem->product_id);
			?>
			<tr>
				<td> {{ $myproduct->name . ' - ' . $myproduct->strength . ' - ' . $myproduct->pack }} </td>   
				<td> {{ $cartitem->qty }} </td>             
			</tr>
			@endforeach
		</tbody>
	</table>
	
	@endforeach


</body>
</html>