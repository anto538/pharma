@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Manage Stock (use negative numbers to reduce stock e.g. -10)</h3>
			<span class="pull-right box-tools">
				<a href="#" class="btn btn-block btn-warning" id="updatestock">Update</a>
			</span>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table class="table table-hover table-responsive dataTable">
					<thead>
						<tr>
							<th>Name</th>
							<th>Current Stock</th>
							<th>Add Stock</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($products as $product)
						<tr>
							<td>{{ $product->name }}</td>
							<td>
								{{ $distributor->products()->where('product_id',$product->id)->first()->pivot->quantity }}	
							</td>
							<td>
								<input type="text" name="stock" id="" class="form-control stock" value="0" data-prodid="{{ $product->id }}" >
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection
@section('foot')
<script>
$(document).ready(function() {
	$('#updatestock').on('click', function(event) {
		event.preventDefault();
		$('.stock').each(function(index, el) {
			console.log($(this).val());
			console.log();
			if ($(this).val() != 0) {
				$.ajax({
			url: 'updatestock',
			type: 'POST',
			data: {
				prodid: $(this).data('prodid'),
				qty: $(this).val(),
				_token: "{{ csrf_token() }}",
		},
		})		.always(function(data) {
			console.log(data);
		});
			};
		});;
			alert("All Stock Updated");
		location.reload(true);
	});;
});
</script>
@stop