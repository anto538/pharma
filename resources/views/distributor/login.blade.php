@extends('layouts.public.master_plain')
@section('content')
<div class="login-box" style="margin:2% auto;">
  <div class="login-logo">
    <img src="{{ URL::to('logo.png') }}" alt="" style="width: 200px; height: auto">
    <a href="#"><b>Pharma</b>Tieup</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Distributor Login</p>
    @if (Session::has('message'))
      <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Notice</strong> {{ Session::get('message')}}
      </div>
    @endif
{!! Form::open(['method' => 'POST', 'url' => '/distributor/login']) !!}
<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
    {!! Form::label('username', 'User ID') !!}
    {!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('username') }}</small>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password',['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('password') }}</small>
</div>
      <div class="row">
      	<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
      		
      	</div>
        <div class="col-xs-6 col-md-6 col-lg-6 col-sm-6 ">
          <div class="checkbox icheck">
           {{--  <label>
              <input type="checkbox"> Remember Me
            </label> --}}
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4 col-md-4 col-lg-4 col-sm-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
{!! Form::close() !!}
    
      <div class="form-group has-feedback">
{{--         <input type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div> --}}


  {{--   <a href="#">I forgot my password</a><br>
    --}}
<a href="{{ route('dregister') }}" class="text-center">Register as a distributor</a> 
  </div>
  <!-- /.login-box-body -->
</div>
@endsection