@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Register With Us </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<table class="table table-hover table-responsive dataTable">
					<thead>
						<tr>
							<th>Name</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($products as $product)
						<tr>
							<td>{{ $product->name }}</td>
							<td><a href="{{ route('distributor.dproduct.edit',$product->id) }}" class="btn btn-warning">EDIT</a>
{!! Form::model($product, ['route' => ['distributor.dproduct.destroy', $product->id], 'method' => 'DELETE', 'onsubmit' => ' return confirm("Do you really want to delete this product ?")']) !!}


		
		{!! Form::submit("DELETE", ['class' => 'btn btn-danger']) !!}

{!! Form::close() !!}
								{{-- <a href="{{ route('distributor.dproduct.edit',$product->id) }}" class="btn btn-danger" onclick="">DELETE</a> --}}

							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection
