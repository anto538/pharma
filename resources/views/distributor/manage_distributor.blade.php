@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Manage Clients </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-hover table-responsive">
       <thead>
         <tr>
           <th>Customer Name</th>
           <th>Email Id</th>
           <th>Actions</th>
           <th>Manage Products</th>
         </tr>
       </thead>
       <tbody>
        @foreach ($distributors as $distributor)
        <tr>
         <td>{{ $distributor->fname . ' ' . $distributor->lname }}</td>
         <td>{{ $distributor->bemail }}</td>
         <td><a href="{{ route('admin.mdistributor.edit',$distributor->id) }}" class="btn btn-warning">EDIT</a></td>
         <td><a href="{{ route('distributorproducts',$distributor->id) }}" class="btn btn-warning">Manage</a></td>
       </tr>
       @endforeach
     </tbody>
   </table>
 </div>
 <!-- /.box-body -->
</div>
</div>
@endsection