@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Register With Us </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if (!isset($edit))
				{!! Form::open(['method' => 'POST', 'route' => 'admin.mbadge.store', 'class' => 'form-horizontal', 'files' => true]) !!}
				@else
				{!! Form::model($badge, ['route' => ['admin.mbadge.update', $badge->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'files' => true]) !!}
				@endif
				
				
				<div class="btn-group pull-right">
					{!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
					{!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
				</div>
				
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection