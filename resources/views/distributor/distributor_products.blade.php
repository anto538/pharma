@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Add product to Distributor : {{ $distributor->bname }} </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				{!! Form::open(['method' => 'POST', 'route' => 'admin.mdistributor.store', 'class' => 'form-horizontal', 'files' => true]) !!}

				{!! Form::hidden('distributor_id', $distributor->id,['id' => 'distributor_id','data-id' =>  $distributor->id]) !!}
				<table class="table table-hover dataTable" id="disprod">
					<thead>
						<tr>
							<th>Product Name</th>
							<th>Select</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($products as $product)
						<tr>
							<td> {{ $product->name }} </td>
							<td>
								<div class="form-group">
									<div class="checkbox{{ $errors->has('selectprod') ? ' has-error' : '' }}">
										<label for="selectprod">
											{!! Form::checkbox('selectprod', $product->id, (!empty($product->distributors()->find($distributor->id))) ? ( !empty($product->distributors()->find($distributor->id)->first()) ? 'checked' : null ) : null, ['class' => 'selectprod']) !!} 
										</label>
									</div>
									<small class="text-danger">{{ $errors->first('selectprod') }}</small>
								</div>
								<span class="action"></span>								
							</td>
						</tr>
						@endforeach	
					</tbody>
				</table>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection
@section('foot')
<script type="text/javascript">
$(document).ready(function() {
	$('#disprod').on('click','.selectprod', function(event) {
		var distributor_id = $('#distributor_id').val();
		var id = $(this).val();
		var toggle = $(this).prop("checked") ? 1 : 0;
		$.ajax({
			url: "{{ route('toggleproduct') }}",
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				id : id,
				toggle : toggle,
				distributor_id : distributor_id
			},
		})
		.always(function(data) {
			console.log(data);
		});
	});;
});
</script>
@stop