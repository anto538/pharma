@foreach ($branches as $branch)
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
	{!! Form::label('branch_id', $branch->name, ['class' => 'col-sm-3 control-label ']) !!}
	{!! Form::hidden('branch_id[]', $branch->id) !!}
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
	<div class="form-group{{ $errors->has('tax_id') ? ' has-error' : '' }}">
		{!! Form::label('tax_id[]', 'Tax Rate:', ['class' => 'col-sm-3 control-label']) !!}
		<div class="col-sm-9">
			{!! Form::select('tax_id[]',$taxes->lists('name','id'), 
				(isset($edit)) ? ((!empty($product->branches()->find($branch->id)->pivot->tax_id)) ? $product->branches()->find($branch->id)->pivot->tax_id : NULL) : NULL
				, ['id' => 'tax_id', 'class' => 'form-control']) !!}
				<small class="text-danger">{{ $errors->first('tax_id') }}</small>
			</div>
		</div>
	</div>
	{{-- ((isset($taxid = $product->branches()->find($branch->id)->pivot->tax_id)) ? $taxid : NULL) --}}
	{{-- {{ dd($product->branches()->find($branch->id)->pivot->tax_id) }} --}}
	@endforeach