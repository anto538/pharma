<table class="table table-hover">
	<thead>
		<tr>
			<th>Branch Name</th>
			<th>#1 MoQ</th>
			<th>#1 Rate (per piece)</th>
			<th>#1 Rewards</th>
			<th>#2 MoQ</th>
			<th>#2 Rate (per piece)</th>
			<th>#2 Rewards</th>
			<th>#3 MoQ</th>
			<th>#3 Rate (per piece)</th>
			<th>#3 Rewards</th>
			<th>#4 MoQ</th>
			<th>#4 Rate (per piece)</th>
			<th>#4 Rewards</th>
			<th>#5 MoQ</th>
			<th>#5 Rate (per piece)</th>
			<th>#5 Rewards</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($branches as $branch)
		<tr>
			<td>{{ $branch->name }} {!! Form::hidden('branchoffer_id[]', $branch->id) !!}</td>

			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer1_moq') ? ' has-error' : '' }}">
						{!! Form::text('boffer1_moq[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer1_moq') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer1_rate') ? ' has-error' : '' }}">
						{!! Form::text('boffer1_rate[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer1_rate') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer1_rewards') ? ' has-error' : '' }}">
						{!! Form::text('boffer1_rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer1_rewards') }}</small>
					</div>
				</div>
			</td>


			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer2_moq') ? ' has-error' : '' }}">
						{!! Form::text('boffer2_moq[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer2_moq') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer2_rate') ? ' has-error' : '' }}">
						{!! Form::text('boffer2_rate[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer2_rate') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer2_rewards') ? ' has-error' : '' }}">
						{!! Form::text('boffer2_rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer2_rewards') }}</small>
					</div>
				</div>
			</td>


			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer3_moq') ? ' has-error' : '' }}">
						{!! Form::text('boffer3_moq[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer3_moq') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer3_rate') ? ' has-error' : '' }}">
						{!! Form::text('boffer3_rate[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer3_rate') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer3_rewards') ? ' has-error' : '' }}">
						{!! Form::text('boffer3_rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer3_rewards') }}</small>
					</div>
				</div>
			</td>


			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer4_moq') ? ' has-error' : '' }}">
						{!! Form::text('boffer4_moq[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer4_moq') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer4_rate') ? ' has-error' : '' }}">
						{!! Form::text('boffer4_rate[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer4_rate') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer4_rewards') ? ' has-error' : '' }}">
						{!! Form::text('boffer4_rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer4_rewards') }}</small>
					</div>
				</div>
			</td>


			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer5_moq') ? ' has-error' : '' }}">
						{!! Form::text('boffer5_moq[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer5_moq') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer5_rate') ? ' has-error' : '' }}">
						{!! Form::text('boffer5_rate[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer5_rate') }}</small>
					</div>
				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group{{ $errors->has('boffer5_rewards') ? ' has-error' : '' }}">
						{!! Form::text('boffer5_rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
						<small class="text-danger">{{ $errors->first('boffer5_rewards') }}</small>
					</div>
				</div>
			</td>


		</tr>
		@endforeach
	</tbody>
</table>