<table class="table table-hover">
	<thead>
		<tr>
			<th>User Name</th>
			<th>User Hidden</th>
			<th>User Price</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($users as $user)
		<tr>
			<td>{{ $user->fname . ' ' . $user->lname }} {!! Form::hidden('user_id[]', $user->id) !!}</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="form-group">
						<div class="checkbox{{ $errors->has('hidden') ? ' has-error' : '' }}">
							<label for="hidden">
								{!! Form::checkbox('uhidden[]', $user->id, NULL, ['id' => 'hidden']) !!} Hide
							</label>
						</div>
						<small class="text-danger">{{ $errors->first('hidden') }}</small>
					</div>

				</div>
			</td>
			<td>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
							{!! Form::text('uprice[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
							<small class="text-danger">{{ $errors->first('price') }}</small>
						</div>
					</div>
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>