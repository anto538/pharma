
@foreach ($branches as $branch)
{!! Form::open(['method' => 'POST', 'route' => 'branchleveloffers', 'class' => 'form-horizontal offer']) !!}

<div class="box box-primary box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">{{ $branch->name }}</h3>
		{!! Form::hidden('branch_id', $branch->id) !!}
		{!! Form::hidden('product_id', $product->id) !!}
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
			</button>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		@foreach ($levels as $level)
		<div class="box box-default box-solid collapsed-box">
			<div class="box-header with-border">
				<h3 class="box-title">{{ $level->name }} {!! Form::hidden('offer_id[]', $level->id) !!}</h3>

				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
					</button>
				</div>
				<!-- /.box-tools -->
			</div>
			<!-- /.box-header -->
			<div class="box-body" style="display: none;">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Offer</th>
							<th>Offer MoQ</th>
							<th>Offer Rate (per piece)</th>
							<th>Offer Rewards</th>
						</tr>
					</thead>
					<tbody>

						@if ($blos->isEmpty())
						@for ($i = 1; $i < 6; $i++)
						<tr>
							<td>Offer #{{ $i }}</td>
							<td>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group{{ $errors->has('offer'.$i.'_moq') ? ' has-error' : '' }}">
										{!! Form::text('offer'.$i.'_moq[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
										<small class="text-danger">{{ $errors->first('offer'.$i.'_moq') }}</small>
									</div>
								</div>
							</td>
							<td>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group{{ $errors->has('offer'.$i.'_rate') ? ' has-error' : '' }}">
										{!! Form::text('offer'.$i.'_rate[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
										<small class="text-danger">{{ $errors->first('offer'.$i.'_rate') }}</small>
									</div>
								</div>
							</td>
							<td>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group{{ $errors->has('offer'.$i.'_rewards') ? ' has-error' : '' }}">
										{!! Form::text('offer'.$i.'_rewards[]', 0, ['class' => 'form-control', 'required' => 'required']) !!}
										<small class="text-danger">{{ $errors->first('offer'.$i.'_rewards') }}</small>
									</div>
								</div>
							</td>
						</tr>
						@endfor
						@else
						@for ($i = 1; $i < 6; $i++)
						<tr>							
							<td>Offer #{{ $i }}</td>
							<td> 
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group{{ $errors->has('offer'.$i.'_moq') ? ' has-error' : '' }}">
										{!! Form::text('offer'.$i.'_moq[]', $blos->where('level_id', $level->id)->first()->{"offer" . $i . "_moq"}, ['class' => 'form-control', 'required' => 'required']) !!}
										<small class="text-danger">{{ $errors->first('offer'.$i.'_moq') }}</small>
									</div>
								</div>
							</td>
							<td>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group{{ $errors->has('offer'.$i.'_rate') ? ' has-error' : '' }}">
										{!! Form::text('offer'.$i.'_rate[]', $blos->where('level_id', $level->id)->first()->{"offer" . $i . "_rate"}, ['class' => 'form-control', 'required' => 'required']) !!}
										<small class="text-danger">{{ $errors->first('offer'.$i.'_rate') }}</small>
									</div>
								</div>
							</td>
							<td>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group{{ $errors->has('offer'.$i.'_rewards') ? ' has-error' : '' }}">
										{!! Form::text('offer'.$i.'_rewards[]', $blos->where('level_id', $level->id)->first()->{"offer" . $i . "_rewards"}, ['class' => 'form-control', 'required' => 'required']) !!}
										<small class="text-danger">{{ $errors->first('offer'.$i.'_rewards') }}</small>
									</div>
								</div>
							</td>
						</tr>
						@endfor

						@endif
						
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		@endforeach

		<div class="btn-group pull-right">
			{!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
		</div>

		{!! Form::close() !!}
	</div>
	<!-- /.box-body -->
</div>

@endforeach
