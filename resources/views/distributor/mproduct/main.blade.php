				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					{!! Form::label('name', 'Product Name') !!}
					{!! Form::text('name', null, ['class' => 'form-control']) !!}
					<small class="text-danger">{{ $errors->first('name') }}</small>
				</div>	
				<div class="form-group{{ $errors->has('cost_price') ? ' has-error' : '' }}">
					{!! Form::label('cost_price', 'Original Price') !!}
					{!! Form::text('cost_price', null, ['class' => 'form-control']) !!}
					<small class="text-danger">{{ $errors->first('cost_price') }}</small>
				</div>
				<div class="form-group{{ $errors->has('base_rewards') ? ' has-error' : '' }}">
					{!! Form::label('base_rewards', 'Base Rewards') !!}
					{!! Form::text('base_rewards', null, ['class' => 'form-control']) !!}
					<small class="text-danger">{{ $errors->first('base_rewards') }}</small>
				</div>
				<div class="form-group{{ $errors->has('warranty') ? ' has-error' : '' }}">
				    {!! Form::label('warranty', 'Warranty In Months') !!}
				    {!! Form::text('warranty',12, ['class' => 'form-control', 'required' => 'required']) !!}
				    <small class="text-danger">{{ $errors->first('warranty') }}</small>
				</div>
				<div class="form-group{{ $errors->has('spec_sheet') ? ' has-error' : '' }}">
					{!! Form::label('spec_sheet', 'Upload Spec Sheet') !!}
					{!! Form::file('spec_sheet') !!}
					<p class="help-block">Upload Your Spec Sheet</p>
					<small class="text-danger">{{ $errors->first('spec_sheet') }}</small>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Product Dimensions</h3>
					</div>
					<div class="panel-body">
						<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
							<div class="form-group{{ $errors->has('length') ? ' has-error' : '' }}">
								{!! Form::label('length', 'Length') !!}
								{!! Form::text('length', null, ['class' => 'form-control']) !!}
								<small class="text-danger">{{ $errors->first('length') }}</small>
							</div>
							<div class="form-group{{ $errors->has('breadth') ? ' has-error' : '' }}">
								{!! Form::label('breadth', 'Breadth') !!}
								{!! Form::text('breadth', null, ['class' => 'form-control']) !!}
								<small class="text-danger">{{ $errors->first('breadth') }}</small>
							</div>

						</div>
						<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 ">
							<div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
								{!! Form::label('height', 'Height') !!}
								{!! Form::text('height', null, ['class' => 'form-control']) !!}
								<small class="text-danger">{{ $errors->first('height') }}</small>
							</div>
							<div class="form-group{{ $errors->has('Weight') ? ' has-error' : '' }}">
								{!! Form::label('Weight', 'Weight') !!}
								{!! Form::text('Weight', null, ['class' => 'form-control']) !!}
								<small class="text-danger">{{ $errors->first('Weight') }}</small>
							</div>	
						</div>
					</div>
				</div>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									Default Offers
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group{{ $errors->has('moffer1_name') ? ' has-error' : '' }}">
											{!! Form::label('moffer1_name', 'Offer #1 Name ') !!}
											{!! Form::text('moffer1_name', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer1_name') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer2_name') ? ' has-error' : '' }}">
											{!! Form::label('moffer2_name', 'Offer #2 Name ') !!}
											{!! Form::text('moffer2_name', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer2_name') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer3_name') ? ' has-error' : '' }}">
											{!! Form::label('moffer3_name', 'Offer #3 Name ') !!}
											{!! Form::text('moffer3_name', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer3_name') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer4_name') ? ' has-error' : '' }}">
											{!! Form::label('moffer4_name', 'Offer #4 Name ') !!}
											{!! Form::text('moffer4_name', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer4_name') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer5_name') ? ' has-error' : '' }}">
											{!! Form::label('moffer5_name', 'Offer #5 Name ') !!}
											{!! Form::text('moffer5_name', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer5_name') }}</small>
										</div>



									</div>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group{{ $errors->has('moffer1_moq') ? ' has-error' : '' }}">
											{!! Form::label('moffer1_moq', 'Offer #1 MOQ') !!}
											{!! Form::text('moffer1_moq', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer1_moq') }}</small>
										</div>
									</div>
									<div class="form-group{{ $errors->has('moffer2_moq') ? ' has-error' : '' }}">
										{!! Form::label('moffer2_moq', 'Offer #2 MOQ') !!}
										{!! Form::text('moffer2_moq', null, ['class' => 'form-control']) !!}
										<small class="text-danger">{{ $errors->first('moffer2_moq') }}</small>
									</div>

									<div class="form-group{{ $errors->has('moffer3_moq') ? ' has-error' : '' }}">
										{!! Form::label('moffer3_moq', 'Offer #3 MOQ') !!}
										{!! Form::text('moffer3_moq', null, ['class' => 'form-control']) !!}
										<small class="text-danger">{{ $errors->first('moffer3_moq') }}</small>
									</div>
									<div class="form-group{{ $errors->has('moffer4_moq') ? ' has-error' : '' }}">
										{!! Form::label('moffer4_moq', 'Offer #4 MOQ') !!}
										{!! Form::text('moffer4_moq', null, ['class' => 'form-control']) !!}
										<small class="text-danger">{{ $errors->first('moffer4_moq') }}</small>
									</div>
									<div class="form-group{{ $errors->has('moffer5_moq') ? ' has-error' : '' }}">
										{!! Form::label('moffer5_moq', 'Offer #5 MOQ') !!}
										{!! Form::text('moffer5_moq', null, ['class' => 'form-control']) !!}
										<small class="text-danger">{{ $errors->first('moffer5_moq') }}</small>
									</div>

								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group{{ $errors->has('moffer1_rate') ? ' has-error' : '' }}">
											{!! Form::label('moffer1_rate', 'Offer #1 Rate') !!}
											{!! Form::text('moffer1_rate', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer1_rate') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer2_rate') ? ' has-error' : '' }}">
											{!! Form::label('moffer2_rate', 'Offer #2 Rate') !!}
											{!! Form::text('moffer2_rate', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer2_rate') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer3_rate') ? ' has-error' : '' }}">
											{!! Form::label('moffer3_rate', 'Offer #3 Rate') !!}
											{!! Form::text('moffer3_rate', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer3_rate') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer4_rate') ? ' has-error' : '' }}">
											{!! Form::label('moffer4_rate', 'Offer #4 Rate') !!}
											{!! Form::text('moffer4_rate', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer4_rate') }}</small>
										</div>
										<div class="form-group{{ $errors->has('moffer5_rate') ? ' has-error' : '' }}">
											{!! Form::label('moffer5_rate', 'Offer #5 Rate') !!}
											{!! Form::text('moffer5_rate', null, ['class' => 'form-control']) !!}
											<small class="text-danger">{{ $errors->first('moffer5_rate') }}</small>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
					{!! Form::label('desc', 'Description') !!}
					{!! Form::textarea('desc', null, ['class' => 'form-control ckeditor']) !!}
					<small class="text-danger">{{ $errors->first('desc') }}</small>
				</div>
				<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
					{!! Form::label('notes', 'Description') !!}
					{!! Form::textarea('notes', null, ['class' => 'form-control ckeditor']) !!}
					<small class="text-danger">{{ $errors->first('notes') }}</small>
				</div>