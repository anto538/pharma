@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Register With Us </h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				@if (!isset($edit))
				{!! Form::open(['method' => 'POST', 'route' => 'admin.mtax.store', 'class' => 'form-horizontal']) !!}
				@else
				{!! Form::model($tax, ['route' => ['admin.mtax.update', $tax->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
				@endif
				
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				    {!! Form::label('name', 'Tax Name') !!}
				    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
				    <small class="text-danger">{{ $errors->first('name') }}</small>
				</div>
				<div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
				    {!! Form::label('rate', 'Tax % / Rate') !!}
				    {!! Form::text('rate', null, ['class' => 'form-control', 'required' => 'required']) !!}
				    <small class="text-danger">{{ $errors->first('rate') }}</small>
				</div>
				<div class="btn-group pull-right">
					{!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
					{!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
				</div>
				
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.box-body -->
	</div>
</div>
@endsection