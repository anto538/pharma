@extends('layouts.distributor.master')
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Register With Us </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if (isset($edit))
        
        {!! Form::model($user, ['route' => ['admin.muser.update', $user->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
        
        @else

        {!! Form::open(['route' => 'admin.muser.store', 'class' => 'form-horizontal']) !!}

        @endif
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Login Details</h3>
          </div>
          <div class="panel-body">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

              <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                {!! Form::label('username', 'Username') !!}
                {!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('username') }}</small>
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('password') }}</small>
              </div>

            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Demo / Package Details</h3>
          </div>
          <div class="panel-body">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group{{ $errors->has('package_name') ? ' has-error' : '' }}">
                {!! Form::label('package_name', 'Package') !!}
                {!! Form::select('package_name',["DEMO"=>"DEMO","1MONTH"=>"1 MONTH","3MONTH"=>"3 MONTH","6MONTH"=>"6 MONTH","12MONTH"=>"1 YEAR"], null, ['id' => 'package_name', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('package_name') }}</small>
              </div>
              <div class="form-group{{ $errors->has('package_expiry') ? ' has-error' : '' }}">
                {!! Form::label('package_expiry', 'Expriy Date') !!}
                {!! Form::date('package_expiry', (isset($edit))? $user->package_expiry->format('Y-m-d'):null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('package_expiry') }}</small>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Your Personal Details</h3>
            </div>
            <div class="panel-body">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                  {!! Form::label('fname', 'First Name') !!}
                  {!! Form::text('fname', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('fname') }}</small>
                </div>
                <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                  {!! Form::label('lname', 'Last Name') !!}
                  {!! Form::text('lname', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('lname') }}</small>
                </div>
                <div class="form-group{{ $errors->has('pan') ? ' has-error' : '' }}">
                  {!! Form::label('pan', 'PAN Number (Business or Personal)') !!}
                  {!! Form::text('pan', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('pan') }}</small>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Business Contact Details</h3>
            </div>
            <div class="panel-body">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group{{ $errors->has('bphone1') ? ' has-error' : '' }}">
                  {!! Form::label('bphone1', 'Landline No #1') !!}
                  {!! Form::number('bphone1', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('bphone1') }}</small>
                </div>
                <div class="form-group{{ $errors->has('bphone2') ? ' has-error' : '' }}">
                  {!! Form::label('bphone2', 'Landline No #2') !!}
                  {!! Form::number('bphone2', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('bphone2') }}</small>
                </div>  
                <div class="form-group{{ $errors->has('bphone3') ? ' has-error' : '' }}">
                  {!! Form::label('bphone3', 'Landline No #3') !!}
                  {!! Form::number('bphone3', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('bphone3') }}</small>
                </div>    
                <div class="form-group{{ $errors->has('bmobile') ? ' has-error' : '' }}">
                  {!! Form::label('bmobile', 'Mobile No') !!}
                  {!! Form::number('bmobile', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('bmobile') }}</small>
                </div>
{{--                 <div class="form-group{{ $errors->has('bfax') ? ' has-error' : '' }}">
                  {!! Form::label('bfax', 'Fax No') !!}
                  {!! Form::text('bfax', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('bfax') }}</small>
                </div> --}}
                <div class="form-group{{ $errors->has('bemail') ? ' has-error' : '' }}">
                  {!! Form::label('bemail', 'Email address') !!}
                  {!! Form::email('bemail', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com']) !!}
                  <small class="text-danger">{{ $errors->first('bemail') }}</small>
                </div>  
              </div>  
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Business Details</h3>
            </div>
            <div class="panel-body">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group{{ $errors->has('bname') ? ' has-error' : '' }}">
                  {!! Form::label('bname', 'Business Name') !!}
                  {!! Form::text('bname', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('bname') }}</small>
                </div>
                <div class="form-group{{ $errors->has('vat') ? ' has-error' : '' }}">
                  {!! Form::label('vat', 'VAT TIN No') !!}
                  {!! Form::text('vat', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('vat') }}</small>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Business Address</h3>
            </div>
            <div class="panel-body">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group{{ $errors->has('baddr1') ? ' has-error' : '' }}">
                  {!! Form::label('baddr1', 'Address #1') !!}
                  {!! Form::text('baddr1', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('baddr1') }}</small>
                </div>
                <div class="form-group{{ $errors->has('baddr2') ? ' has-error' : '' }}">
                  {!! Form::label('baddr2', 'Address #2') !!}
                  {!! Form::text('baddr2', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('baddr2') }}</small>
                </div>
                <div class="form-group{{ $errors->has('baddr3') ? ' has-error' : '' }}">
                  {!! Form::label('baddr3', 'Address #3') !!}
                  {!! Form::text('baddr3', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('baddr3') }}</small>
                </div>
                <div class="form-group{{ $errors->has('baddr4') ? ' has-error' : '' }}">
                  {!! Form::label('baddr4', 'Address #4') !!}
                  {!! Form::text('baddr4', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('baddr4') }}</small>
                </div>
                <div class="form-group{{ $errors->has('bcity') ? ' has-error' : '' }}">
                  {!! Form::label('bcity', 'City') !!}
                  {!! Form::text('bcity', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('bcity') }}</small>
                </div>
                <div class="form-group{{ $errors->has('bstate') ? ' has-error' : '' }}">
                  {!! Form::label('bstate', 'State') !!}
                  {!! Form::text('bstate', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('bstate') }}</small>
                </div>
                <div class="form-group{{ $errors->has('bcountry') ? ' has-error' : '' }}">
                  {!! Form::label('bcountry', 'Country') !!}
                  {!! Form::text('bcountry','India', ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('bcountry') }}</small>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

          <div class="form-group{{ $errors->has('internal_notes') ? ' has-error' : '' }}">
            {!! Form::label('internal_notes', 'Any Message or Information you wish to state') !!}
            {!! Form::textarea('internal_notes', null, ['class' => 'form-control ckeditor']) !!}
            <small class="text-danger">{{ $errors->first('internal_notes') }}</small>
          </div>
          
        </div>
        <div class="clearfix"></div>
        {!! Form::submit("Save", ['class' => 'btn btn-success btn-block']) !!}
        <div class="btn-group ">
        </div>
        
        {!! Form::close() !!}
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</div>
@endsection