<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->string('fname');
            $table->string('lname');
            $table->string('phone1');
            $table->string('phone2');
            $table->string('mobile');
            $table->string('email');
            $table->string('pan');
            $table->string('addr1');
            $table->string('addr2');
            $table->string('addr3');
            $table->string('addr4');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
