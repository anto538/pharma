<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DistributorProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->string('scheme');
            $table->decimal('purchase_rate',10,2);
            $table->decimal('mrp',10,2);
            $table->decimal('tax',10,2);
            $table->decimal('vat',10,2);
            $table->decimal('cash_discount',10,2);
            $table->decimal('net_amount',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('distributor_product');
    }
}
