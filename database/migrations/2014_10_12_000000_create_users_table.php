<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('username')->unique();
            $table->string('password');            
            $table->string('pan');
            $table->string('bname');
            $table->string('bphone1');
            $table->string('bphone2');
            $table->string('bphone3');
            $table->string('bmobile');
            $table->string('bfax');
            $table->string('bemail');
            $table->string('vat');
            $table->string('baddr1');
            $table->string('baddr2');
            $table->string('baddr3');
            $table->string('baddr4');
            $table->string('bcity');
            $table->string('bstate');
            $table->string('bcountry');
            $table->mediumtext('internal_notes');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
