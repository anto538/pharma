<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

use App\Cart;
use App\CartItem;
use App\Brand;
use App\Distributor;
use App\Product;
use Auth;

class ManageProductController extends Controller
{
  public function __construct()
  {
    $this->middleware('admin');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $product = Product::all();
      return view('admin.manage_product')->with([
        'products' => $product,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $brands = Brand::all();


      return view('admin.product')->with([
        'brands' => $brands,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all()); 
      $input = $request->all();
      $input = $request->except('_token');
      Product::create($input);
      return redirect()->route('admin.mproduct.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $product = Product::find($id);
     $brands = Brand::all();
     return view('admin.product')->with([
      'product' => $product,
      'edit' => 'edit',  
      'brands' => $brands,
      ]); 
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $product = Product::find($id);
      $input = $request->all();
      $input = $request->except('_token');
           $product->fill($input)->save();

     return redirect()->route('admin.mproduct.index');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function postImgUpload(Request $request){
      $destinationPath = 'product_images';
      $extension = Input::file('file')->getClientOriginalExtension(); 
      $fileName = time().rand(0,9999).'_product_image.'.$extension; 
      Input::file('file')->move($destinationPath, $fileName); 
      $input['image_path'] = $destinationPath.'/'.$fileName;
      $input['prod_id'] = $request->header('prod-id');
      ProductImage::create($input);
      return response()->json(['success' => 200, 'data' => $input,'filename' =>  $fileName]);
    }
    public function postImgDelete(Request $request)
    {
      $destinationPath = 'product_images/';
      ProductImage::where('image_path',$destinationPath . Input::get('file'))->delete();
      unlink($destinationPath . Input::get('file'));
      return response()->json(['success' => 200 ]);
    }
    public function postImgDeleteById(Request $request)
    {
// return $request->all();
      $destinationPath = 'product_images/';
      $image = ProductImage::find($request['id']);
      unlink($image->image_path);
      $image->delete();

    }
        public function getDistributorList($id)
    {
        $distributors = Distributor::all();
        $product = Product::find($id);
        return view('admin.product_distributors')->with([
            'distributors' => $distributors,
            'product' => $product,
            ]);
    }
    public function postToggleDistributor(Request $request)
    {
        $input = $request->all();
        $product = Product::find($input['product_id']);
        if ($input['toggle'] > 0) {
            $product->distributors()->attach($input['id']);
        } else {
            $product->distributors()->detach($input['id']);
        }
        
        return response()->json($request->all());
    }
   

  }
