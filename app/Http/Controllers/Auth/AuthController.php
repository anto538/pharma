<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Hash;
use Session;
use Carbon\Carbon;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/store';
    protected $redirectAfterLogout = '/login';
    protected $loginPath = '/register';
    protected $redirectPath = '/store';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
          'email' => 'required|email|unique:users',
          'passwordx' => 'required',
          ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            ]);
    }
    public function showLoginForm()
    {
        if (Auth::check()) {
           return redirect()->url('/store');
        } else {
           return view('public.login');
        }
        
        
    }
    public function postLogin(Request $request)
    {
        $user = $request->input('username');
        $pw = $request->input('password');
        if (Auth::attempt(['username' => $user, 'password' => $pw]))
        {
            if (Carbon::now()->lte(Auth::user()->package_expiry)) {
                return redirect('store');
                dd(Auth::user()); 
            } else {
             Auth::logout();
                return redirect('/login');
         }
     }
     return redirect('/login');

 }
 public function logout()
 {
    if (Auth::check()) {
        Auth::logout();
    }
    return redirect('/');
}
public function getForgotPassword()
{
    return view('public.forgotpass');
}
public function postForgotPassword(Request $request)
{
    $input = $request->all();
    $user = User::where(['username'=>$input['username'],'bemail'=>$input['bemail'],'pan'=>strtoupper($input['pan'])])->first();
    if (!empty($user)) {
        return view('public.resetpass')->with('user',$user);
    } else {
        Session::flash('message','Incorrect Details');
        return redirect()->route('forgotpassword')->with(['message'=>'Incorrect Details']);
    }
}
public function postResetPassword(Request $request)
{
    $input = $request->all();
    $user = User::where(['id'=>$input['id'],'username'=>$input['username'],'bemail'=>$input['bemail'],'pan'=>strtoupper($input['pan'])])->first();
    $user->password = Hash::make($input['password']);
    $user->save();
    return redirect('/login');
}
}
