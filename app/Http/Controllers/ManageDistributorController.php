<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Distributor;
use App\Product;
class ManageDistributorController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * 
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $distributors = Distributor::all();
        return view('admin.manage_distributor')->with([
            'distributors' => $distributors,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.distributor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['bemail'] = trim($input['bemail']);
        $input['password'] = bcrypt($input['password']);

        $distributor = Distributor::create($input);
        return redirect()->route('distributorproducts', $distributor->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $distributor = Distributor::find($id);
       return view('admin.distributor')->with([
        'distributor' => $distributor,
        'edit' => 'edit',
        ]);
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $distributor = Distributor::find($id);
$input['bemail'] = trim($input['bemail']);
        $input['password'] = bcrypt($input['password']);
        if ($distributor->password === $input['password']) {
            unset($input['password']);
        } 
        $distributor->fill($input)->save();

        return redirect()->route('distributorproducts', $distributor->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getProductsList($id)
    {
        $distributor = Distributor::find($id);
        $products = Product::all();
        return view('admin.distributor_products')->with([
            'distributor' => $distributor,
            'products' => $products,
            ]);
    }
    public function postToggleProduct(Request $request)
    {
        $input = $request->all();
        $distributor = Distributor::find($input['distributor_id']);
        if ($input['toggle'] > 0) {
            $distributor->products()->attach($input['id']);
        } else {
            $distributor->products()->detach($input['id']);
        }
        
        return response()->json($request->all());
    }
}
