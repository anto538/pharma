<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RegisterFormRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mail;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('public.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterFormRequest $request)
    {
        $input = $request->all();
        $input = $request->except('_token','commission');
        // dd($input); 
        $input['password'] = Hash::make($input['password']);
        $input['package_expiry'] = Carbon::now()->addDays(3000); 
        User::create($input);
        return redirect()->route('store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['username'=> $request['username'],'password'=> $request['password'] ])) {
    // dd($request);
            return redirect()->route('store');
        }
    }
    public function getHelp()
    {
        return view('public.help')->with(['user'=> Auth::user()]);
    }
    public function postHelp(Request $request)
    {
        $input = $request->all();
        $user = Auth::user();
        Mail::send('email.help', ['user' => $user,'input' => $input], function ($m) use ($user) {
            $m->from('info@pharmatieup.com', 'PharmaTieup App');

            $m->to(getenv('ADMIN_MAIL'), 'PharmaTieup App')->subject('Help Request!');
        });
         return redirect()->route('store');
    }

}
