<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Distributor;
use App\Product;
use Session;
class DistributorController extends Controller
{
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('distributor.distributor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['bemail'] = trim($input['bemail']);
        $input['password'] = bcrypt($input['password']);

        $distributor = Distributor::create($input);
        Session::flash('message', 'Registeration was Successfull!'); 
        return redirect()->route('distributorhome');
    }
}
