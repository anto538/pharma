<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Cart;
use App\CartItem;
use App\Distributor;
use App\Product;
use Auth;
class StoreController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function getProductsList($id='')
	{
		$cartitems = array();
		$mycartitems = array();
		$user = array();
		if (Auth::check()) {
			$user = Auth::user();
			$cart = Cart::where(['user_id' => $user->id , 'status' => 'CART'])->first();
			if (!empty($cart)) {
				$cartitems = CartItem::where(['user_id' => $user->id,'cart_id' => $cart->id])->get();
			}
			$i = 0;
			foreach ($cartitems as $cartitem) {
				$prod = Product::find($cartitem->product_id);
				$mycartitems[$i]['id']= $cartitem->id;
				$mycartitems[$i]['pname']= $prod->name . "(".$prod->brand()->first()->name.")" . ' - ' . $prod->strength . ' - ' . $prod->pack;
				$mycartitems[$i]['bname']= Distributor::find($cartitem->distributor_id)->bname;
				$mycartitems[$i]['qty']= $cartitem->qty;
				$mycartitems[$i]['price']=$cartitem->total;
				$mycartitems[$i]['count']= CartItem::where('cart_id',$cart->id)->count();
				$i++;
			}
		} else {
			
		}

		return view('public.shop')->with([
			'user' => $user,
			'cart' => $cart,
			'cartitems' => $cartitems,
			'mycartitems' => $mycartitems,
			]);
	}
}
