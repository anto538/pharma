<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

use App\Cart;
use App\CartItem;
use App\Brand;
use App\Distributor;
use App\Product;
use Auth;

class DistributorProductController extends Controller
{
  // public function __construct()
  // {

  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $distributor = Auth::guard('distributor')->user();

      $product = Product::all();
      $product = $distributor->products()->get();
      $brands = Brand::all();
// $brands = Brand::whereIn('id',$distributor->products()->get()->lists('brand_id'))->get();
      return view('distributor.brand_product')->with([
        'products' => $product,
        'brands' => $brands,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $brands = Brand::all();


      return view('distributor.product')->with([
        'brands' => $brands,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all()); 
      $input = $request->all();
      $input = $request->except('_token');
      if (!empty($input['brand_id'])) {
        $brand = Brand::find($input['brand_id']);
      } else {
        $brand = Brand::create(["name"=>$input['brand_name']]);
        $input['brand_id'] = $brand->id;
      }
      if (!empty($input['product_id'])) {
        $product = Product::find($input['product_id']);
      } else {
        unset($input['product_id']);
        $product = Product::create($input);
      }
      $product->distributors()->attach(Auth::guard('distributor')->user()->id,[
        'quantity' => $input['quantity'],
        'scheme' => $input['scheme'],
        'purchase_rate' => $input['purchase_rate'],
        'mrp' => $input['mrp'],
        'tax' => $input['tax'],
        'vat' => $input['vat'],
        'cash_discount' => $input['cash_discount'],
        'net_amount' => $input['net_amount']
        ]);

      return redirect()->route('distributor.dproduct.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $distributor = Auth::guard('distributor')->user();

      $product = Product::all();
      $product = $distributor->products()->where("brand_id",$id)->get();
      $brands = Brand::all();
      return view('distributor.manage_product')->with([
        'products' => $product,
        'brands' => $brands,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $product = Product::find($id);
     $distributor = Auth::guard('distributor')->user();

     $brands = Brand::all();
     // $myproduct = $distributor->products()->where("product_id",$product->id)->first()->pivot;
     return view('distributor.product')->with([
      'product' => $product,
      'distributor' => $distributor,
      'edit' => 'edit',  
      'brands' => $brands,
      // 'myproduct' => $myproduct,
      // 'distributor' => $distributor,
      ]); 
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $product = Product::find($id);
      $input = $request->all();
      $input = $request->except('_token');
      if (!empty($input['brand_id'])) {
        $brand = Brand::find($input['brand_id']);
      } else {
        $brand = Brand::create(["name"=>$input['brand_name']]);
        $input['brand_id'] = $brand->id;
      }
      $product->fill($input)->save();
      $product->distributors()->detach(Auth::guard('distributor')->user()->id);
      $product->distributors()->attach(Auth::guard('distributor')->user()->id,[
        'quantity' => $input['quantity'],
        'scheme' => $input['scheme'],
        'purchase_rate' => $input['purchase_rate'],
        'mrp' => $input['mrp'],
        'tax' => $input['tax'],
        'vat' => $input['vat'],
        'cash_discount' => $input['cash_discount'],
        'net_amount' => $input['net_amount']
        ]);
      return redirect()->route('distributor.dproduct.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $product = Product::find($id);
        // dd($product->distributors()->where('distributor_id',Auth::guard('distributor')->user()->id)->first()->pivot); 
      $product->distributors()->detach(Auth::guard('distributor')->user()->id);
      return redirect()->route('distributor.dproduct.index');
    }

    public function manageStock()
    {
     $distributor = Auth::guard('distributor')->user();
     $product = $distributor->products()->get();
     $brands = Brand::all();
     return view('distributor.manage_product_stock')->with([
      'distributor' => $distributor,
      'products' => $product,
      'brands' => $brands,
      ]);
   }
   public function updatestock(Request $request)
   {
    $input = $request->all();
      $distributor = Auth::guard('distributor')->user();
      $product = $distributor->products()->where("product_id",$input['prodid'])->first()->pivot;
        $product->quantity += $input['qty'];
        $product->save();
         return response()->json($product);
   }
 }
