<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Brand;
use App\Cart;
use App\CartItem;
use App\Distributor;
use App\Product;
use Auth;
use Mail;
use DB;
use Carbon\Carbon;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mycart = array();
        if (Auth::check()) {
            $user = Auth::user();
            $cart = Cart::where('user_id',$user->id)->first();
            $cartitems = CartItem::where('cart_id',$cart->id)->get();
            $products = Product::whereIn('id',$cartitems->lists('product_id'))->get();
            $product_images = ProductImage::whereIn('prod_id',$products->lists('id'))->get();
            // $subcategories = SubCategory::whereIn('id',$products->lists('product_id'))->get();
            $categories = Category::all();
            $taxes = Tax::all();
            // $subcategories = SubCategory::all();

        }
        return view('public.cart')->with([
            'products' => $products,
            'product_images' => $product_images,
            'categories' => $categories,
            'taxes' => $taxes,
            'user' => $user,
            'cart' => $cart,
            'cartitems' => $cartitems,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if (Auth::check()) {
            $user = Auth::user();
            $data['qty'] = $input['qty'];
            if ($data['qty'] > 0) {
                $cart = Cart::firstOrCreate(['user_id' => $user->id, 'status' => 'CART']);
                $data['cart_id'] = $cart->id;

                $data['user_id'] = $user->id;
                $data['distributor_id'] = $input['distributor_id'];
                $data['product_id'] = $input['product_id'];
                // $data['rate'] = Product::find($input['product_id'])->rate;               
                $data['rate'] = Distributor::find($input['distributor_id'])->products()->where('product_id', $data['product_id'])->first()->pivot->net_amount;               
                $data['tax'] = (float)  round(($data['qty'] *  Distributor::find($input['distributor_id'])->products()->where('product_id', $data['product_id'])->first()->pivot->vat),2);
                $data['total'] = (float)  round(($data['qty'] *  $data['rate']),2);

                $cartitem = CartItem::create($data);

   // $cartitem = CartItem::firstOrNew($data);
   //              $data['rate'] = Product::find($input['product_id'])->rate;               
   //              $data['total'] = (float)  round(($data['qty'] *  $data['rate']),2);
   //              $cartitem->fill($data)->save();

                $cart->total = CartItem::where('cart_id',$data['cart_id'])->sum('total');
                $cart->save();

                $item = array();
                $item['bname']= Distributor::find($input['distributor_id'])->bname;
                if (!empty(Product::find($data['product_id'])->brand()->first())) {
                   $item['pname']= Product::find($data['product_id'])->name . "(".Product::find($data['product_id'])->brand()->first()->name .")";
               } else {
                   $item['pname']= Product::find($data['product_id'])->name ;
               }
               
               
               $item['qty']= $data['qty'];
               $item['price']= $data['total'];
               $item['count']= CartItem::where('cart_id',$cart->id)->count();
               $item['total']= $cart->total;
               $item['cartid']= $cart->id;
               $item['id']= $cartitem->id;
               return  response()->json($item);
           } else {
              return null;
          }

      } else {
        return null;
    }
    return null;
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_token');
        $cart_item = CartItem::find($input['item_id']);
        $cart = Cart::find($cart_item->cart_id);
        $data = array();
        $response = array();
        $data['qty'] = $input['qty'];
        // $data['rate'] = $cart_item->rate;
        if ($data['qty'] > 0) { 
         $tax_rate = Tax::find(Product::find($cart_item->product_id)->branches->find($cart_item->branch_id)->pivot->tax_id)->first()->rate;
         $sale = (float) round(($data['qty'] *  $cart_item->rate),2);
         $response['tax'] = $data['tax'] =(float)  round((($tax_rate / 100) * $sale),2);
         $response['total'] = $data['total'] = (float)  round(($sale +  $data['tax']),2);

         $cart_item->fill($data)->save();

         $response['cart_tax'] = $cart->tax =  CartItem::where('cart_id',$cart->id)->sum('tax');
         $temp_total = CartItem::where('cart_id',$cart->id)->sum('total');
         $response['cart_shipping'] = $cart->shipping = (float) round(((1 / 100) * $temp_total),2);
         $response['cart_total'] = $cart->total = $cart->shipping +  $temp_total;
         $cart->save();
     }
     return response()->json($response);
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart_item = CartItem::find($id);
        $cart_item->delete();
        $cart = Cart::find($cart_item->cart_id);
        $cart->tax =  CartItem::where('cart_id',$cart->id)->sum('tax');
        $temp_total = CartItem::where('cart_id',$cart->id)->sum('total');
        $cart->shipping = (float) round(((1 / 100) * $temp_total),2);
        $cart->total = $cart->shipping +  $temp_total;
        $cart->save();
        return response()->json($cart_item);
    }
    public function getDistributorsList()
    {
        $term = Input::get('term');
        $product_id = Input::get('product_id');
        $product = Product::find($product_id);
        // return response()->json($product);
     //    if (!empty($term)) {
     //     $distributors = $product->distributors()->where('bname','LIKE','%'.$term.'%')->get();
     // } else {
     //     $distributors = $product->distributors()->get();
     // }

$distributor_products = DB::table('distributor_product')->where('product_id',$product->id)->get();
// $distributors = Distributor::whereIn('id',$distributor_products->lists('distributor_id'))->get();

     $i = 0;
     $mydistributors = array();

     foreach ($distributor_products as $dp) {
        $distributor = Distributor::where('id',$dp->distributor_id)->first();
       //  if ($distributor->products()->where("product_id",$product->id)->first()->pivot->quantity <= 0) {
       //     continue;
       // }
        if ($dp->quantity <= 0) {
           continue;
       }
       $mydistributors[$i]['id'] =  $distributor->id;
       $mydistributors[$i]['label'] =  $distributor->bname;
       if (!empty($dp->scheme)) {
           $mydistributors[$i]['label'] .=' - Scheme:' . $dp->scheme; 
       }

       $mydistributors[$i]['label'] .=' - [PR]: Rs.' . $dp->net_amount .' - [MRP]: Rs.' . $dp->mrp .' - AVAILABLE:' . $dp->quantity . " nos.";
       $mydistributors[$i]['value'] =  $distributor->bname;
       $mydistributors[$i]['stock'] = $dp->quantity;
       // if (!empty($distributor->products()->where("product_id",$product->id)->first()->pivot->scheme)) {
       //     $mydistributors[$i]['label'] .=' - Scheme:' . $distributor->products()->where("product_id",$product->id)->first()->pivot->scheme; 
       // }

       // $mydistributors[$i]['label'] .=' - [PR]: Rs.' . $distributor->products()->where("product_id",$product->id)->first()->pivot->net_amount .' - [MRP]: Rs.' . $distributor->products()->where("product_id",$product->id)->first()->pivot->mrp .' - AVAILABLE:' . $distributor->products()->where("product_id",$product->id)->first()->pivot->quantity . " nos.";
       // $mydistributors[$i]['value'] =  $distributor->bname;
       // $mydistributors[$i]['stock'] = $distributor->products()->where("product_id",$product->id)->first()->pivot->quantity;
       $i++;
   }
   return response()->json($mydistributors);
}

public function getProductsList()
{
    $term = Input::get('term');
    $brand_id = Input::get('bid');
    $brand = Brand::find($brand_id);
    if (!empty($brand)) {
        $products = Product::where('name','LIKE','%'.$term.'%')->where('brand_id',$brand->id)->get();
    } else {
     $products = Product::where('name','LIKE','%'.$term.'%')->get();
 }
 $i = 0;
 foreach ($products as $product) {
    $myproducts[$i]['id'] =  $product->id;
    $myproducts[$i]['strength'] =  $product->strength;
    $myproducts[$i]['pack'] =  $product->pack;
    $myproducts[$i]['label'] =  $myproducts[$i]['value'] = $product->name. " - " . $product->pack ;
    $i++;
}
return response()->json($myproducts);
}
public function getProductsList2()
{
    $term = Input::get('term');
    $brand_id = Input::get('bid');
    $brand = Brand::find($brand_id);
    if (!empty($brand)) {
        $products = Product::where('name','LIKE','%'.$term.'%')->where('brand_id',$brand->id)->get();
    } else {
     $products = Product::where('name','LIKE','%'.$term.'%')->get();
 }
 $i = 0;
 foreach ($products as $product) {
    $myproducts[$i]['id'] =  $product->id;
    $myproducts[$i]['strength'] =  $product->strength;
    $myproducts[$i]['pack'] =  $product->pack;
    if ($product->brand()->first()) {
     $myproducts[$i]['label'] =  $myproducts[$i]['value'] = $product->name . "(".$product->brand()->first()->name.")" . " - " . $product->pack ;
 } else {
     $myproducts[$i]['label'] =  $myproducts[$i]['value'] = $product->name . " - " . $product->pack;
 }
 $i++;
}
return response()->json($myproducts);
}

public function getBrandsList()
{
    $term = Input::get('term');
    $brands = Brand::where('name','LIKE','%'.$term.'%')->get();
    $i = 0;
    foreach ($brands as $brand) {
        $mybrands[$i]['id'] =  $brand->id;
        // $myproducts[$i]['label'] =  $myproducts[$i]['value'] = $product->name . ' - ' . $product->strength . ' - ' . $product->pack. ' - Rs.' . $product->rate  ;
        $mybrands[$i]['label'] =  $mybrands[$i]['value'] = $brand->name ;
        $i++;
    }
    return response()->json($mybrands);
}

public function checkout(Request $request)
{
    $input = $request->all();
    if (Auth::check()) {
        $user = Auth::user();
        $cart = Cart::where(['user_id' => $user->id , 'status' => 'CART'])->first(); 
        $cart->status = "CART";
        $cart->save();
        $cartitems = null;
        if (!empty($cart)) {
            $cartitems = CartItem::where(['user_id' => $user->id,'cart_id' => $cart->id])->get();
        }
        $products = Product::whereIn('id',$cartitems->lists('product_id'))->get();
        $distributors = Distributor::whereIn('id',$cartitems->lists('distributor_id'))->get();

    }
    return view('public.checkout')->with([
        'user' => $user,
        'cart' => $cart,
        'cartitems' => $cartitems,
        'products' => $products,
        'distributors' => $distributors,
        ]);
}
public function complete(Request $request)
{
    $input = $request->all();

    if (Auth::check()) {
        $user = Auth::user();
        $cart = Cart::where(['user_id' => $user->id , 'status' => 'CART', "id" => $input['checkout_id']])->first(); 
        $cart->status = "CHECKOUT";
        $cart->save();
        $cartitems = null;
        if (!empty($cart)) {
            $cartitems = CartItem::where(['user_id' => $user->id,'cart_id' => $cart->id])->get();
        }
        $distributors = Distributor::whereIn('id',$cartitems->lists('distributor_id'))->get();

        foreach ($distributors as $distributor) {
            foreach ($cartitems as $cartitem) {
                if (!empty($distributor->products()->where("product_id",$cartitem->product_id)->first())) {
                   $dproduct = $distributor->products()->where("product_id",$cartitem->product_id)->first()->pivot;
                   $dproduct->quantity -= $cartitem->qty;
                   $dproduct->save();
               }
               
           }
           $products = $distributor->products()->whereIn('product_id',$cartitems->lists('product_id'))->get();
             // Mail to Distributor
           // dd($distributor); 
           Mail::send('email.billinglist_distributor', ['user' => $user,'products' => $products,'cartitems' => $cartitems,'distributor' => $distributor ], function ($message) use ($distributor , $user) {
            $message->from('do-not-reply@pharmatieup.com', 'PharmaTieUp');
            $message->to($distributor->bemail)->subject('New Order By ' . $user->bname .' on ' . Carbon::now()->subDay()->format('d-m-Y'));
        }); 
       }
       $products = Product::all();
        // Mail to USER
       Mail::send('email.billinglist_user', ['user' => $user,'products' => $products,'cartitems' => $cartitems,'distributors' => $distributors], function ($message) use ($user) {
        $message->from('do-not-reply@pharmatieup.com', 'PharmaTieUp');
        $message->to($user->bemail)->subject('Your Order was placed successfully on ' . Carbon::now()->subDay()->format('d-m-Y'));
    }); 
        // Mail to ADMIN
       Mail::send('email.billinglist_admin', ['user' => $user,'products' => $products,'cartitems' => $cartitems,'distributors' => $distributors], function ($message) use ($user) {
        $message->from('do-not-reply@pharmatieup.com', 'PharmaTieUp');
        $message->to(getenv('ADMIN_MAIL'))->subject('Order by ' . $user->bname .' on ' . Carbon::now()->subDay()->format('d-m-Y'));
    }); 

   }
   return redirect()->route('store');
}

}
