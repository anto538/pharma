<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//NAMED ROUTES
Route::get('/',[
    'uses' => 'Auth\AuthController@showLoginForm',
    'as' => 'home'
    ]);

Route::get('register', [
	'uses' => 'UserController@index',
	'as' => 'register'
	]);

$this->get('login', 'Auth\AuthController@showLoginForm');
$this->get('logout', 'Auth\AuthController@logout')->name('userlogout');
$this->post('login', 'Auth\AuthController@postLogin');
$this->get('forgotpassword', 'Auth\AuthController@getForgotPassword')->name('forgotpassword');
$this->post('forgotpassword', 'Auth\AuthController@postForgotPassword');
$this->post('resetpass', 'Auth\AuthController@postResetPassword')->name('resetpass');;


Route::get('store/{id?}',[
    'uses' => 'StoreController@getProductsList',
    'as' => 'store'
    ]);



//RESOURCE ROUTES PUBLIC
Route::resource('user', 'UserController');
Route::resource('cart', 'CartController');
Route::get('distributors/', [
    'uses' => 'CartController@getDistributorsList',
    'as' => 'getdistributors'
    ]);
Route::get('products/', [
    'uses' => 'CartController@getProductsList',
    'as' => 'getproducts'
    ]);
Route::get('products2/', [
    'uses' => 'CartController@getProductsList2',
    'as' => 'getproducts2'
    ]);
Route::get('brands/', [
    'uses' => 'CartController@getBrandsList',
    'as' => 'getbrands'
    ]);
Route::post('checkout/', [
    'uses' => 'CartController@checkout',
    'as' => 'checkout'
    ]);
Route::post('complete/', [
    'uses' => 'CartController@complete',
    'as' => 'complete'
    ]);
Route::get('gethelp/', [
    'uses' => 'UserController@getHelp',
    'as' => 'gethelp'
    ]);
Route::post('gethelp/', [
    'uses' => 'UserController@postHelp',
    'as' => 'gethelp'
    ]);
//RESOURCE ROUTES ADMIN

Route::group(['prefix' => 'admin'], function() {
    Route::get('/',[
        'uses' => 'AdminAuth\AuthController@showLoginForm',
        'as' => 'adminhome'
        ]);
    $this->get('login', 'AdminAuth\AuthController@showLoginForm');
    $this->post('login', 'AdminAuth\AuthController@postLogin');
    $this->get('logout', 'AdminAuth\AuthController@logout')->name('adminlogout');
    Route::resource('muser', 'ManageUserController');
    Route::resource('mdistributor', 'ManageDistributorController');
    Route::resource('mbrand', 'ManageBrandController');
    Route::resource('mproduct', 'ManageProductController');
    Route::resource('mtax', 'ManageTaxController');
    Route::get('distributorproducts/{id}', [
        'uses' => 'ManageDistributorController@getProductsList',
        'as' => 'distributorproducts'
        ]);
    Route::post('toggleproduct', [
        'uses' => 'ManageDistributorController@postToggleProduct',
        'as' => 'toggleproduct'
        ]);
    Route::get('productdistributors/{id}', [
        'uses' => 'ManageProductController@getDistributorList',
        'as' => 'productdistributors'
        ]);
    Route::post('toggledistributor', [
        'uses' => 'ManageProductController@postToggleDistributor',
        'as' => 'toggledistributor'
        ]);
});
Route::group(['prefix' => 'distributor'], function() {
   $this->get('login', 'DistributorAuth\AuthController@showLoginForm')->name('distributorlogin');
   $this->post('login', 'DistributorAuth\AuthController@postLogin');
   $this->get('logout', 'DistributorAuth\AuthController@logout')->name('distributorlogout');
   Route::get('/',[
    'uses' => 'DistributorAuth\AuthController@showLoginForm',
    'as' => 'distributorhome'
    ]);
   Route::get('/dashboard',[
    'uses' => 'DistributorAuth\AuthController@dashboard',
    'as' => 'distributordashboard'
    ]);
   Route::get('/register',[
    'uses' => 'DistributorController@create',
    'as' => 'dregister'
    ]);
   Route::post('/register',[
    'uses' => 'DistributorController@store',
    'as' => 'dregister'
    ]);
   Route::get('/managestock',[
    'uses' => 'DistributorProductController@manageStock',
    'as' => 'managestock'
    ]);
   Route::post('/updatestock',[
    'uses' => 'DistributorProductController@updateStock',
    'as' => 'updatestock'
    ]);

   // Route::resource('distributor', 'DistributorController');
   Route::resource('dproduct', 'DistributorProductController');


   // Route::get('products/', [
   //  'uses' => 'DistributorProductController@getProductsList',
   //  'as' => 'getproducts'
   //  ]);
});
// Route::auth();


