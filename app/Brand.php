<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
	protected $fillable = [
	'name',
	'desc',
	];
/**
 * Brand has many Products.
 *
 * @return \Illuminate\Database\Eloquent\Relations\HasMany
 */
public function products()
{
	// hasMany(RelatedModel, foreignKeyOnRelatedModel = brand_id, localKey = id)
	return $this->hasMany('App\Product');
}
}
