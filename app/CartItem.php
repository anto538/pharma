<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
	protected $fillable = [
	'cart_id',
	'user_id',
	'distributor_id',
	'product_id',
	'qty',
	'tax',
	'rate',
	'total',
	];
	public function cart()
	{
		$this->belongsTo('App\Cart','cart_id');
	}
	/**
	 * CartItem belongs to Distributor.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function distributor()
	{
		// belongsTo(RelatedModel, foreignKey = distributor_id, keyOnRelatedModel = id)
		return $this->belongsTo('App\Distributor');
	}
	/**
	 * CartItem belongs to Product.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product()
	{
		// belongsTo(RelatedModel, foreignKey = product_id, keyOnRelatedModel = id)
		return $this->belongsTo('App\Product');
	}
}
