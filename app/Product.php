<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
	'brand_id',
	'name',
	'strength',	
	'pack',	
	'scheme',	
	'rate',	
	];
	/**
	 * Product belongs to Brand.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function brand()
	{
		// belongsTo(RelatedModel, foreignKey = brand_id, keyOnRelatedModel = id)
		return $this->belongsTo('App\Brand');
	}
	/**
	 * Product belongs to Distributors.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function distributors()
	{
		// belongsTo(RelatedModel, foreignKey = distributors_id, keyOnRelatedModel = id)
		return $this->belongsToMany('App\Distributor', 'distributor_product', 'product_id', 'distributor_id')->withPivot('quantity','purchase_rate','mrp','vat','tax','cash_discount','net_amount','scheme');
	}
}
