<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	protected $fillable = [
	'user_id',
	'total',
	'status',
	];
	public function users()
	{
		$this->belongsTo('App\User');
	}
	public function cartitems()
	{
		$this->hasMany('App\CartItem');
	}

}
