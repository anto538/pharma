<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Distributor extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'fname',
    'lname',
    'username',
    'password',
    'bname',
    'bphone1',
    'bphone2',
    'bphone3',
    'bmobile',
    'bfax',
    'bemail',
    'vat',
    'baddr1',
    'baddr2',
    'baddr3',
    'baddr4',
    'bcity',
    'bstate',
    'bcountry',
    'internal_notes',
    'bank_name',
    'bank_accname',
    'bank_type',
    'bank_branch',
    'bank_ifsc',
    'drug_lic_no',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];
   /**
    * Distributor belongs to Products.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function products()
   {
       // belongsTo(RelatedModel, foreignKey = products_id, keyOnRelatedModel = id)
       return $this->belongsToMany('App\Product', 'distributor_product', 'distributor_id','product_id')->withPivot('quantity','purchase_rate','mrp','vat','tax','cash_discount','net_amount','scheme');
   }
 }
