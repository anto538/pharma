<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
	protected $fillable = [
	'username',
	'password',
	'fname',
	'lname',
	'phone1',
	'phone2',
	'mobile',
	'email',
	'pan',
	'addr1',
	'addr2',
	'addr3',
	'addr4',
	'city',
	'state',
	'country',
	];


	// protected $guard = "admin";
}
