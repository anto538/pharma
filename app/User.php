<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'fname',
    'lname',
    'username',
    'password',
    'bname',
    'bphone1',
    'bphone2',
    'bphone3',
    'bmobile',
    'pan',
    'bfax',
    'bemail',
    'vat',
    'baddr1',
    'baddr2',
    'baddr3',
    'baddr4',
    'bcity',
    'bstate',
    'bcountry',
    'internal_notes',
    'package_name',
    'package_expiry',
    'drug_lic_no',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];
    protected $dates = ['package_expiry'];
/**
 * User has one Cart.
 *
 * @return \Illuminate\Database\Eloquent\Relations\HasOne
 */
public function cart()
{
    // hasOne(RelatedModel, foreignKeyOnRelatedModel = user_id, localKey = id)
    return $this->hasOne('App\Cart');
}
}
