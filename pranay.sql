-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 30, 2018 at 07:05 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pranay`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addr1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addr2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addr3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addr4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `fname`, `lname`, `phone1`, `phone2`, `mobile`, `email`, `pan`, `addr1`, `addr2`, `addr3`, `addr4`, `city`, `state`, `country`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$4ZThUO6nrNeGUR825I9JK.y89VPW72sMrMfmNcU5Pk7Pcf76gq4AG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ss2rrbXuM8HQIgL7LHASqS8Z7JtlQiwsEtVj6AVFp6pXTqmCGkop3oUGlzqp', NULL, '2016-10-09 05:10:50');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `shipping` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distributors`
--

CREATE TABLE `distributors` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bphone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bphone2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bphone3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bmobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bfax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bemail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bcity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bstate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bcountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `internal_notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_accname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_branch` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_ifsc` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distributor_product`
--

CREATE TABLE `distributor_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2016_08_02_120051_create_admins_table', 1),
('2016_08_05_062403_create_brands_table', 1),
('2016_08_05_062439_create_products_table', 1),
('2016_08_05_062458_create_taxes_table', 1),
('2016_08_05_065910_create_carts_table', 1),
('2016_08_05_065922_create_cart_items_table', 1),
('2016_09_27_100126_create_distributors_table', 2),
('2016_09_28_064638_distributor_product', 3),
('2016_12_03_043548_UpdateDistributor', 4),
('2016_12_10_060926_UpdateUser', 5);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `strength` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pack` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bphone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bphone2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bphone3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bmobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bfax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bemail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baddr4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bcity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bstate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bcountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `internal_notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `package_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `package_expiry` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `username`, `password`, `pan`, `bname`, `bphone1`, `bphone2`, `bphone3`, `bmobile`, `bfax`, `bemail`, `vat`, `baddr1`, `baddr2`, `baddr3`, `baddr4`, `bcity`, `bstate`, `bcountry`, `internal_notes`, `remember_token`, `created_at`, `updated_at`, `package_name`, `package_expiry`) VALUES
(1, 'Antonius', 'Carvalho', 'anto', '$2y$10$xv26EqQOh4VuLd3A6r4dY.z8CjBi.k8rkH6C7jPoshi6AzebXdYDq', '', 'Antonius Carvalho', '09773991234', '09773991234', '09773991234', '21', '9773991234', 'info@exits.in', 'asd', 'Carvalho House Carvalho Nagar', '', '', '400606', 'Thane', 'Maharashtra', 'India', '<p>reference Keerthan</p>\r\n\r\n<p>OK Verified</p>\r\n', 'Qj77d6A1YWL7qH67ccFIIns4aRTt66ecFecZv7t1eY1KJPr1QfvXJ3bRHAIJ', NULL, '2017-02-02 01:52:12', '', '2016-12-26'),
(2, 'Antonius', 'Carvalho', 'anto538', '$2y$10$V9kNaxTzoDFRyA9GUOmAWOugi7.X5JMvFaLzwlLsqlp56.ZR2eeAy', 'ANMPC2373H', 'EXiTS', '', '', '', '2', '', 'info@exits.in', 'ANMPC2373H', 'Carvalho House', 'Carvalho Nagar', '', '', 'Thane', 'Maharashtra', 'IN', '', NULL, '2016-10-08 04:43:45', '2016-12-10 23:44:31', '', '2016-12-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributors`
--
ALTER TABLE `distributors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `distributors_username_unique` (`username`);

--
-- Indexes for table `distributor_product`
--
ALTER TABLE `distributor_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distributors`
--
ALTER TABLE `distributors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `distributor_product`
--
ALTER TABLE `distributor_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
